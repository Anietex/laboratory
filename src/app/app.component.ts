import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute, ActivatedRouteSnapshot } from  '@angular/router';
import { SettingService  } from './settings/setting.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
 constructor(private setting: SettingService){}

 ngOnInit(){
 	if(!this.setting.getLocalSettings()){
 		
 	}
 }


 getSettings(){
 	this.setting.getSettings().subscribe((data)=>{
 			this.setting.saveLocalSettings(data);	
 		},(error)=>{
 			// this.ngOnInit();
 	});
 }

}
