import { MedicalMicrobiologyFormComponent } from './medical-microbiology-form/medical-microbiology-form.component';
import  {ViewChild } from '@angular/core';
import MiniToast from '../../MiniToast';

export  abstract class AbstractMedicalMicrobiologyComponent {
	  
	@ViewChild(MedicalMicrobiologyFormComponent) reportForm;
	saving = false;
	toast = new MiniToast();
	
	public resetForm(){
		this.reportForm.reset();
	}
	
}
