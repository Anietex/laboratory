import { Component, OnInit } from '@angular/core';
import { IMedicalMicrobiology } from '../imedical-microbiology';
import { MedicalMicrobiologyService } from '../medical-microbiology.service';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-medical-microbiology-view',
  templateUrl: './medical-microbiology-view.component.html',
  styles: []
})
export class MedicalMicrobiologyViewComponent implements OnInit {
	public report: IMedicalMicrobiology;
  loadingReport = false;
    conError = false;
    serverError = false;
    id = null;
  constructor(private route: ActivatedRoute, private serviceProvider:MedicalMicrobiologyService) { 
     this.getReport = this.getReport.bind(this)
  }

  ngOnInit(){
  	

     this.route.params.subscribe((param)=>{
        this.id = param.id;
        this.getReport()
      });
  }
 






   getReport(){
       this.loadingReport = true;
       this.conError = false;
       this.serverError = false;


        this.serviceProvider.getReport(this.id).subscribe((report)=>{
             this.loadingReport = false;
             const keys = Object.keys(report);

             for(const key of keys){

                 if( report[key] === null  || report[key] == 'null' ){
                     report[key]='';
                 }else if( report[key]===''){
                     report[key]='';
                 }
             }
            this.report = report as IMedicalMicrobiology;
            

         },(err)=>{
            this.loadingReport = false;
           if(err.status == 0){
               this.conError = true;
           }else{
             this.serverError = true;
           }
         }
         );

  }

}
 