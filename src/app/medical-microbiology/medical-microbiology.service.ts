import { Injectable, Inject } from '@angular/core';
import {IMedicalMicrobiology } from './imedical-microbiology';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MedicalMicrobiologyService {

  constructor(@Inject('API_URL') private apiUrl: string,private http: HttpClient) { }


  createReport(report: IMedicalMicrobiology,options){
  	 const formData = new FormData();
        let fields :string[] = Object.keys(report);
        for(const field of fields){
            if(field=='id') continue;
            formData.append(field,report[field]);
        }

      return  this.http.post(this.apiUrl+'medical-microbiology',formData,{params:options});
  }

  getReports(){
      return this.http.get(this.apiUrl+'medical-microbiology');
  }


  deleteReport(id: number){
       const formData = new FormData();
           formData.append('_method',"DELETE");
           return this.http.post(this.apiUrl+'medical-microbiology/'+id,formData);
  }


  getReport(id: number){
        return this.http.get(this.apiUrl+'medical-microbiology/'+id)
    }

    updateReport(report:IMedicalMicrobiology){
        const formData = new FormData();
        formData.append('_method','PATCH');
        let fields :string[] = Object.keys(report);
        for(const field of fields){
            if(field=='id') continue;
            formData.append(field,report[field]);
        }

        return  this.http.post(this.apiUrl+'medical-microbiology/'+report.id,formData)
    }

  
}
 