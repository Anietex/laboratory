import { Component, OnInit,AfterViewInit, EventEmitter,Output,Input, AfterViewChecked,OnChanges } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AbstractCreateReport } from '../../shared/abstract-create-report';
import { Router, ActivatedRoute } from '@angular/router';
import {PatientService } from '../../patient/patient.service';
import {IPatient } from '../../patient/ipatient';
import {AbstractPaneComponent } from '../../shared/abstract-pane-component';
import {IMedicalMicrobiology  } from '../imedical-microbiology';
import  Minitoast from '../../../MiniToast';
import {AbstractReportForm } from '../../shared/abstract-report-form';
declare let M:any;


@Component({
  selector: 'med-microbiology-form',
  templateUrl: './medical-microbiology-form.component.html',
  styleUrls: ['./medical-microbiology-form.component.scss']
})
export class MedicalMicrobiologyFormComponent extends AbstractReportForm implements OnInit,AfterViewChecked {

	medMicroForm = new FormGroup({
		patient_id: new FormControl(''),
 		requested_by: new FormControl(),
 		clinical_detail: new FormControl(),
 		exam_required: new FormControl(),
 		specimen: new FormControl(),
 		clinic: new FormControl(),
		antibiotic_therapy: new FormControl(''),
        macroscopy:new FormControl(''),
        cell_count: new FormControl(''),
        differential_wbc:new FormControl(''),
        pus_cell:new FormControl(''),
        rbc:new FormControl(''),
        epithelial_cell:new FormControl(''),
        gram_pus_cell:new FormControl(''),
        gve_ccoci_pos:new FormControl(''),
        gve_rods_pos:new FormControl(''),
        gve_ccoci_neg:new FormControl(''),
        gve_rods_neg:new FormControl(''),
        gram_epithelial_cell:new FormControl(''),
        zn_stain_pus_cell:new FormControl(''),
        zn_stain_afb_x1: new FormControl(''),
        zn_stain_afb_x2: new FormControl(''),
        zn_stain_afb_x3: new FormControl(''),
        test_time: new FormControl(''),
        abstinence: new FormControl(''),
        volume: new FormControl(''),
        colour: new FormControl(''),
        liquefaction: new FormControl(''),
        viscosity: new FormControl(''),
        ph: new FormControl(''),
        viability: new FormControl(''),
        motility_active: new FormControl(''),
        motility_sluggish: new FormControl(''),
        motility_non_motile: new FormControl(''),
        count: new FormControl(''),
        hpf_pus_cell: new FormControl(''),
        morph_pus_cell: new FormControl(''),
        hpf_rbc:new FormControl(''), 
        morph_rbc: new FormControl(''),
        hpf_epithelial_cell:new FormControl(''),
        morph_epithelial_cell: new FormControl(''),
        stool_macroscopy: new FormControl(''),
        wet_preparation: new FormControl(''),
        concentration: new FormControl(''),
        malaria_parasite: new FormControl(''),
        microfilaria_skin:new FormControl(''),
        microfilaria_blood: new FormControl(''),
        stool_occult_blood: new FormControl(''),
        vdrl_test: new FormControl(''),
        aso_titre: new FormControl(''),
        chlamydia_serology: new FormControl(''),
        hepbag: new FormControl(''),
        hcv_ab: new FormControl(''),
        helicobacter:  new FormControl(''),
        retroviral_screening:new FormControl(''),
        retroviral_confirmation:new FormControl(''),
        cd4_count: new FormControl(''),
        given: new FormControl(''),
        read:new FormControl(''),
        result:new FormControl(''),
        s_typhi_o:new FormControl(''),
        s_typhi_h: new FormControl(''),
        s_paratyphi_a_o: new FormControl(''),
        s_paratyphi_a_h: new FormControl(''),
        s_paratyphi_b_o: new FormControl(''),
        s_paratyphi_b_h: new FormControl(''),
        s_paratyphi_c_o: new FormControl(''),
        s_paratyphi_c_h: new FormControl(''),
        comment: new FormControl(''),
        organism_isolated:new FormControl(),
        amoxycillin:new FormControl(''),
        cefuroxime:new FormControl(''),
        ceftriaxone:new FormControl(''),
        ceftazidime: new FormControl(''),
        cotrimoxazole:new FormControl(''),
        norfloxacin:new FormControl(''),
        tebacylin:new FormControl(''),
        nalidixic_acid: new FormControl(''),
        streptomacycin: new FormControl(''),
        septrin: new FormControl(''),
        ciprofloxin:new FormControl(''),
        chloramphenicol:new FormControl(''),
        erythromycin: new FormControl(''),
        gentymacin: new FormControl(''),
        ofloxacin: new FormControl(''),
        nitrofuratoin: new FormControl(''),
        ampiclox: new FormControl(''),
        amoxycilin: new FormControl(''),
        clindamycin:new FormControl(''),
        levofloxcin:new FormControl(''),
	});

	@Input() report: IMedicalMicrobiology;
 	@Output() submit =  new EventEmitter<IMedicalMicrobiology>();
 	@Input() saveBtnText = "Save Report";
    @Input() saving : boolean;


 constructor(protected router: Router, protected patientService: PatientService,protected activeRoute: ActivatedRoute){
 		super(router,patientService,activeRoute);
 		
 	}



   ngOnChanges(){
  	if(this.report){
  		this.medMicroForm.patchValue({...this.report})
  	 }
   }


  reset(){
		this.medMicroForm.reset();
		setTimeout(()=>{this.initMaterial(); M.updateTextFields();},200);
		this.activePane = 0;
		this.showPane();
	}



	showPane(){
		if(this.activePane>5){
			this.showNextBtn = false;
			this.showSave = true;
		}else{
			this.showNextBtn = true;
			this.showSave = false;

		}
		 if(this.activePane >0 ){
			this.showPrevBtn = true;
		}else{
			this.showPrevBtn = false;
		}
	}



	showTextInput(event: Event){
       event.preventDefault();
       let td: HTMLElement=event.currentTarget as HTMLElement;
        if(td.lastChild instanceof Text){
            td.removeChild(td.lastChild)
        }
       td.querySelector('label').style.display = 'none';
       td.querySelector('input').style.display='block';
       td.querySelector('input').focus();

    }

    hideTextInput(event: Event){
        let input:HTMLInputElement = event.currentTarget as HTMLInputElement;
        let td:HTMLElement = input.parentElement;
        if(input.value.length!=0){
            td.appendChild(document.createTextNode(input.value));
            td.querySelector('label').style.display = 'none';
            td.querySelector('input').style.display='none';
        }else {
            td.querySelector('label').style.display = 'block';
            td.querySelector('input').style.display='none';
        }


    }


    ngAfterViewInit(){
        super.ngAfterViewInit();
        let cells = document.getElementsByClassName('table-field');
        [].map.call(cells,cell=>{
          let td= cell as HTMLElement;
          let input =  td.querySelector('input') as HTMLInputElement;

          if(input.value.length>0){
              td.appendChild(document.createTextNode(input.value));
              td.querySelector('label').style.display = 'none';
              td.querySelector('input').style.display='none';
          }

        });

    }


    handleSubmit(){
    	let patient_id = Number.parseInt(this.medMicroForm.value.patient_id)
		if(isNaN(patient_id)){
			new Minitoast().warning("You have not seleted any patient");
			this.activePane = 0;
			this.showPane();
		}else{
			this.submit.emit(this.medMicroForm.value)
		}
    }

}
