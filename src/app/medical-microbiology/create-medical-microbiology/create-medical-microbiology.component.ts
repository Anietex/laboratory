import { Component, OnInit, ViewChild } from '@angular/core';
import  {IMedicalMicrobiology } from '../imedical-microbiology';
import { AbstractMedicalMicrobiologyComponent } from '../abstract-medical-microbiology-component';
import { MedicalMicrobiologyService } from '../medical-microbiology.service';
import {ModalComponent } from '../../shared/modal/modal.component';
import { SettingService} from '../../settings/setting.service'; 
import { AfterSaveActions } from '../../shared/AfterSaveActions'; 


@Component({
  selector: 'app-create-medical-microbiology',
  templateUrl: './create-medical-microbiology.component.html',
  styleUrls: ['./create-medical-microbiology.component.scss']
})
export class CreateMedicalMicrobiologyComponent  extends AbstractMedicalMicrobiologyComponent implements OnInit {
 
 @ViewChild(ModalComponent) modal : ModalComponent;
  
  constructor(private serviceProvider: MedicalMicrobiologyService, private setting: SettingService){
 	super();
  }

  ngOnInit() {
  }


  onSubmit(report: IMedicalMicrobiology){
  	 let settings = this.setting.getLocalSettings().after_save_action 
    if(settings == AfterSaveActions.ALWAYS_ASK){
       this.modal.openModal((options)=>{
         if(options){
           this.saveReport(report,options);
         }else{
            this.saveReport(report,{});
         }
       });
    }else if(settings == AfterSaveActions.SMS_PATIENT){
        let options = {
          sms:true
        }
         this.saveReport(report,options);

        this.saveReport(report,options);
    }else if(settings  == AfterSaveActions.EMAIL_PATIENT){
        let options = {
          email:true
        }
    }else if(settings == AfterSaveActions.EMAIL_AND_SMS_PATIENT){
       let options = {
          sms:true,
          email:true
        }
         this.saveReport(report,options);
    }else{
       this.saveReport(report,{});
    }
    
  }



  saveReport(report,options){
    this.saving = true;
    this.serviceProvider.createReport(report,options).subscribe(
      (data)=>{
        this.saving = false;
        this.toast.success('Report was saved successfully')
        this.resetForm();
      },
      (error)=>{
        this.saving = false;
        if(error.status ==0){
          this.toast.error("Can't connect to server please try again");
        }else{
          this.toast.error("An error occurred report could not be saved");
        }
      }
      )
    
  }
}
