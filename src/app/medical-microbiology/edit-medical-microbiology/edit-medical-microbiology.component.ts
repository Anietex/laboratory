import { Component, OnInit, ViewChild } from '@angular/core';
import  {IMedicalMicrobiology } from '../imedical-microbiology';
import { AbstractMedicalMicrobiologyComponent } from '../abstract-medical-microbiology-component';
import { MedicalMicrobiologyService } from '../medical-microbiology.service';
import {ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-edit-medical-microbiology',
  templateUrl: './edit-medical-microbiology.component.html',
  styleUrls: ['./edit-medical-microbiology.component.scss']
})
export class EditMedicalMicrobiologyComponent extends AbstractMedicalMicrobiologyComponent implements OnInit {

  id= null;
  report :  IMedicalMicrobiology;
  conError = false;
  serverError = false;
  loadingReport = false;


  constructor(private serviceProvider: MedicalMicrobiologyService, private route: ActivatedRoute ) {
  		super();
      this.getReport = this.getReport.bind(this);
   }

  ngOnInit(){

  	this.route.params.subscribe((params)=>{
  		this.id = params.id;
  		this.getReport();
  	})
  }


  onSubmit(report: IMedicalMicrobiology){
  	this.saving = true;
    report.id = this.id;
    this.report = report;


  	this.serviceProvider.updateReport(report).subscribe(
  		(data)=>{
  			this.saving = false;
  			this.toast.success('Report was updated successfully')
  		},
  		(error)=>{
  			this.saving = false;
  			if(error.status == 0){
  				this.toast.error("Can't connect to server please try again");
  			}else{
  				this.toast.error("An error occurred report could not be updated");
  			}
  		}
  		)
  	
  }



  getReport(){
  	this.loadingReport = true;
  	this.conError = false;
  	this.serverError = false;
  	this.serviceProvider.getReport(this.id).subscribe((report)=>{
      console.log(report);
      const keys = Object.keys(report);

             for(const key of keys){
                 if( report[key] === null  || report[key] == 'null' ){
                     report[key]='';
                 }else if( report[key]===''){
                     report[key]='';
                 }
             }
  		this.report = report as IMedicalMicrobiology;
  		this.loadingReport = false;
  	},
  	(err)=>{
  		this.loadingReport = false;
  		if(err.status == 0){
  			this.conError = true;
  		}else{
  			this.serverError = true;
  		}
  	})
  }

}
 