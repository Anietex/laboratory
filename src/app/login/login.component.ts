import { Component, OnInit } from '@angular/core';
import {SettingService } from '../settings/setting.service';
import {AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import MiniToast from '../../MiniToast';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private setting: SettingService, private auth: AuthService, protected router: Router ) { }

  	logoUrl= '';
  	logginIn = false;
  	toast = new MiniToast();

	  ngOnInit(){
	  		if(this.setting.getLocalSettings()){
	  			this.logoUrl=this.setting.getLocalSettings().logo_file;
	  		}

        if(this.auth.isLogin()){
          this.router.navigateByUrl('/dashboard');
        }
	  }


  login(ev){
  	this.logginIn = true;
  	this.auth.login(new FormData(ev.target)).subscribe((res)=>{

      if(!this.setting.getLocalSettings()){
        this.setting.getSettings().subscribe((data)=>{
            this.setting.saveLocalSettings(data);
            this.redirect(res);
        },()=>{
          this.logginIn = false;
            this.toast.error("An error occured please try again");
        });
      }else{
          this.redirect(res);

      }
  	},
  	(error)=>{
  		this.logginIn = false;
  		if(error.status == 401){
  			this.toast.error("Invalid Username or password")
  		}else if(error.status == 0){
  			this.toast.error("Can't connect to server");
  		}else{
  			this.toast.error("An error occured please try again");
  		}
  	
  	})
  }



  redirect(res){
    this.logginIn = false;
      let data = res as any;
      this.auth.setToken(data.token)
      this.auth.setUser(data.user);
      this.toast.success("Log in was successfull");
      this.auth.onLogIn.emit();
      this.router.navigateByUrl('/dashboard');
  }

}
