import { Component, OnInit,ViewChild } from '@angular/core';
import { RadiologicalRequestService } from '../services/radiological-request.service';
import MiniToast from '../../MiniToast';
import {IRadiologicalRequest} from '../interfaces/iradiological-request';
import {MatPaginator, MatTableDataSource, MatTable,MatSort} from '@angular/material';



declare var roar:  any;

@Component({
  selector: 'app-radiological-request-list',
  templateUrl: './radiological-request-list.component.html',
  styleUrls: ['./radiological-request-list.component.scss']
})
export class RadiologicalRequestListComponent implements OnInit {
	errorOccurred = true;
	loadingRequests = false;
	deleting = false;

     @ViewChild(MatPaginator) paginator: MatPaginator;
     @ViewChild(MatSort) sort: MatSort;
     matDatasource = new MatTableDataSource();
     columnsToDisplay = ['id','patient_name','age','sex','view','edit','delete'];




     @ViewChild(MatSort) set content(content: MatSort) {
      this.sort = content;
    if (this.sort){
         this.matDatasource.sort = this.sort;

     }
   }


   @ViewChild(MatPaginator) set pager(content: MatPaginator) {
      this.paginator = content;
    if (this.paginator){
         this.matDatasource.paginator = this.paginator;

     }
   }

	 requests: IRadiologicalRequest[];

  constructor(private serviceProvider: RadiologicalRequestService) { }

  ngOnInit() {
    this.getRequests()
    this.matDatasource.paginator = this.paginator;
    this.matDatasource.sort = this.sort;
  }

  getRequests(){
  	this.errorOccurred = false;
  	this.loadingRequests = true;
  	this.serviceProvider.getRequests().subscribe(
  		(data)=>{
  			this.loadingRequests = false;
  			this.requests = data as IRadiologicalRequest[];
            this.matDatasource.data = this.requests;

  		},
  		(err)=>{
  			this.loadingRequests = false;
  			this.errorOccurred = true;

  		}
  		)
  }

  deleteRequest(id,index){
       const toast = new MiniToast();


       var options = {
       cancel: true,
       cancelText: 'Cancel',
       cancelCallBack: function (event) {},
       confirm: true,
        confirmText: 'Delete',
        confirmCallBack: (event)=> {
            this.deleting = true;
            this.serviceProvider.deleteRequest(id).subscribe((res)=>{
                toast.success('Request was deleted succesfully');
                this.deleting = false;
                this.requests.splice(index,1);
            },
            (err)=>{
                toast.error("Request could not be deleted at the moment please try again")
            }
            )
       }
      }

        roar('Delete Request', 'Do you realy want to  <strong>Delete </strong> this request', options);
    }

    filterTable(searchText: string){
        this.matDatasource.filter = searchText.trim().toLowerCase();
    }



}
