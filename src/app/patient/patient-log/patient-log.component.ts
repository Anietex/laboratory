import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router' ;
import {PatientService } from '../patient.service';
import { AbstractTableComponent } from '../../shared/abstract-table-component';
import {MatPaginator, MatTableDataSource, MatTable,MatSort} from '@angular/material';

@Component({
  selector: 'app-patient-log',
  templateUrl: './patient-log.component.html',
  styleUrls: ['./patient-log.component.scss']
})

export class PatientLogComponent extends AbstractTableComponent implements OnInit {


	loading=false;
	serverError = false;
	conError = false;
    deleting = false;
    id=null;
    columnsToDisplay = ['type','date','view'];
     matDatasource = new MatTableDataSource<any>();


    logs = [];

  constructor(private serviceProvider: PatientService, private route: ActivatedRoute ){ 
  		super();

  		this.getPatientLog = this.getPatientLog.bind(this);


  }

  ngOnInit() {
  	this.route.params.subscribe((param)=>{
  		this.id = param.id;
  		this.getPatientLog()
  	})
  }


  getPatientLog(){
  	this.loading = true;
  	this.conError =false;
  	this.serverError = false;
  	this.serviceProvider.getPatientLog(this.id).subscribe(
  		(data)=>{
  			this.logs = data as any;
        this.matDatasource.data = this.logs;

  			this.loading = false;
  		},
  		(error)=>{
  			this.loading = false;
  			if(error.status ==0){
  				this.conError = true;
  			}else{
  				this.serverError = true;
  			}

  		});
  }

  getLink(log){

  	switch (log.type) {
  		case "Medical Microbiology":
  			return '/medical-microbiology-and-parasitology/reports/'+log.id;
  		case "Haematology":
  			return '/haematology/reports/'+log.id;
  		case "Chemical pathology":
  		  return '/chemical-pathology/reports/'+log.id;
  	}
  }

}
