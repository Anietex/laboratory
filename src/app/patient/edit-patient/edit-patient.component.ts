import { Component, OnInit } from '@angular/core';
import {ActivatedRoute } from '@angular/router';
import {IPatient } from '../ipatient';
import { PatientFormComponent } from '../patient-form/patient-form.component';
import {PatientService } from '../patient.service';
import { AbstractPatientComponent } from '../abstract-patient-component';


@Component({
  selector: 'app-edit-patient',
  templateUrl: './edit-patient.component.html',
  styles: []
})
export class EditPatientComponent extends AbstractPatientComponent implements OnInit {

	id:number;
	conError = false;
	serverError = false;
	loadingPatient = false;
	patient: IPatient;
	saveBtnText="Update";

    constructor(private route: ActivatedRoute, private serviceProvider:PatientService ) { super()}

  ngOnInit() {

  	this.route.params.subscribe((params)=>{
  		this.id = params.id;
  		this.getPatient();
  	})
  }


  getPatient(){
  	this.serverError = false;
  	this.loadingPatient = true;
  	this.conError = false;
  	this.serviceProvider.getPatient(this.id).subscribe(
  	(patient)=>{
  		this.loadingPatient = false;
  		this.patient = patient as IPatient;
  		
  	},
  	(err)=>{
  		this.loadingPatient = false;
  		if(err.status ==0)
  			this.conError = true;
  		else
  			this.serverError = true;

  	})
  }

 

  handleSubmit(patientData:IPatient){
  	this.loading = true;
  	this.patient = patientData;
  	patientData.id = this.id;

  	this.serviceProvider.updatePatient(patientData).subscribe(
  		(data)=>{
  			this.loading = false;
  			// this.patientForm.resetForm();
  			this.toast.success("Patient data was saved successfully");
  		},

  		(error)=>{
  			this.loading = false;
  			if(error.status ==0){
  				this.toast.error("Can't connect to the server please try again")
  			}else{
  				this.toast.error("An error occured patient data could not be saved, please try again");
  			}
  			
  		}
  		);
  }

}
