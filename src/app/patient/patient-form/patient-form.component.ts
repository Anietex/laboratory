import {
    Component,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChange,
    SimpleChanges,
    AfterViewChecked,
    AfterViewInit, Input
}  from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms'
import { IPatient} from '../ipatient';
declare var M:any;
@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styles: []
})
export class PatientFormComponent implements OnInit,OnChanges, AfterViewChecked {

	public patientForm = new FormGroup({
       surname:  new FormControl(''),
       first_name:  new FormControl(''),
       middle_name:  new FormControl(''),
       age:  new FormControl(''),
       sex:  new FormControl(''),
       requested_by:  new FormControl(''),
       email:  new FormControl(''),
       phone_no:new FormControl(''),
       exam_required:  new FormControl(''),
       specimen:  new FormControl(''),
       clinic:  new FormControl(''),
       clinical_detail: new FormControl('')
	});

 

	@Output() submit = new EventEmitter<IPatient>();
	@Input()  patient: IPatient;
  @Input() disableBtn : boolean = false;
  @Input()  saveBtnText = "Save";

    constructor() { }

    ngOnInit() {

    }

    onSubmit(){
    	this.submit.emit(this.patientForm.value)
    }

    resetForm(){
    	this.patientForm.reset();
    }

    ngOnChanges(){
      this.patientForm.patchValue({...this.patient})
    }

    ngAfterViewChecked(){
       M.updateTextFields();
    }

}
 