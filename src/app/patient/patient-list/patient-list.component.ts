import { Component, OnInit,ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatTable,MatSort} from '@angular/material';
import {PatientService } from '../patient.service';
import {IPatient } from '../ipatient';
import {AbstractPatientComponent } from '../abstract-patient-component';
import { AbstractTableComponent } from '../../shared/abstract-table-component';


@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styles: []
})
export class PatientListComponent extends AbstractTableComponent implements OnInit {

	loading=false;
	serverError = false;
	conError = false;
  deleting = false;
  patients: IPatient[];

	


    matDatasource = new MatTableDataSource<IPatient>();
    columnsToDisplay = ['id','firstName','middleName','surname','age','phone_no','email','viewTests','edit','delete'];


    


  constructor(private serviceProvider: PatientService) { 
  	super(); 
  	this.getPatients = this.getPatients.bind(this);
  }

  ngOnInit(){
  		this.getPatients();
  	
  }


  getPatients(){
  	this.loading = true;
  	this.conError =false;
  	this.serverError = false;
  	this.serviceProvider.getPatients().subscribe(
  		(data)=>{
  			this.patients = data as IPatient[];
        this.matDatasource.data = this.patients;

  			this.loading = false;
  		},
  		(error)=>{
  			this.loading = false;
  			if(error.status ==0){
  				this.conError = true;
  			}else{
  				this.serverError = true;
  			}

  		});
  }



  deletePatient(id,index){
    var options = {
       cancel: true,
       cancelText: 'Cancel',
       cancelCallBack: function (event) {},
       confirm: true,
        confirmText: 'Delete',
        confirmCallBack: (event)=> {
            this.deleting = true;
            this.serviceProvider.deletePatient(id).subscribe((res)=>{
                this.toast.success('Patient record was deleted succesfully');
                this.deleting = false;
                this.patients.splice(index,1);
                this.matDatasource.data = this.patients;
            },
            (err)=>{
                this.deleting = false;
                this.toast.error("Patient record could not be deleted at the moment please try again")
            }
            )
       }
    }

        this.roar('Delete Patient record', 'Do you realy want to  <strong>Delete </strong> this record', options);
    }
  }


