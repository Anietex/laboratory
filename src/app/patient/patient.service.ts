import { Injectable,Inject } from '@angular/core';
import { IPatient } from './ipatient';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class PatientService {

  constructor(@Inject('API_URL') private apiUrl: string,private http: HttpClient) { }

  createPatient(patientData: IPatient){
  	const formData = new FormData();
        let fields :string[] = Object.keys(patientData);
        for(const field of fields){
            if(field=='id') continue;
            formData.append(field,patientData[field]);
        }

      return  this.http.post(this.apiUrl+'patient',formData)
  }

  updatePatient(patientData: IPatient){
    const formData = new FormData();
        formData.append('_method','PATCH');
        let fields :string[] = Object.keys(patientData);
        for(const field of fields){
            if(field=='id') continue;
            formData.append(field,patientData[field]);
        }

        return  this.http.post(this.apiUrl+'patient/'+patientData.id,formData)
  }

  getPatients(){
  	return this.http.get(this.apiUrl+'patient');
  }

  getPatient(id:number){
     return this.http.get(this.apiUrl+'patient/'+id)
  }


  getPatientLog(id:number){
     return this.http.get(this.apiUrl+'patient/logs/'+id)
  }

  deletePatient(id:number){
    const formData = new FormData();
    formData.append('_method',"DELETE");
    return this.http.post(this.apiUrl+'patient/'+id,formData);
  }

}
 