import { Component, OnInit,ViewChild } from '@angular/core';
import {IPatient } from '../ipatient';
import { PatientFormComponent } from '../patient-form/patient-form.component';
import {PatientService } from '../patient.service';
import { AbstractPatientComponent } from '../abstract-patient-component';
import {ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-patient',
  templateUrl: './create-patient.component.html',
  styleUrls: ['./create-patient.component.scss']
})

export class CreatePatientComponent extends AbstractPatientComponent implements OnInit {

	  next:string ;
    constructor(private serviceProvider: PatientService, private route: ActivatedRoute, private router: Router ){ 
       super();
      this.route.queryParamMap.subscribe((paramMap)=>{
          let param = paramMap as any
          if(param.params.r){
            let next = param.params.r;

            if(/^(\/.+)+\/.+/ig.test(next)){
             this.next = next;
            }
          }
         
      })

    	super();
    }

  ngOnInit() {

  }


  handleSubmit(patientData:IPatient){
  	this.loading = true;

  	this.serviceProvider.createPatient(patientData).subscribe(
  		(data)=>{
  			this.loading = false;
  			this.patientForm.resetForm();
  			this.toast.success("Patient data was saved successfully");

        if(this.next){
          this.router.navigateByUrl(this.next);
        }
  		},

  		(error)=>{
  			this.loading = false;
  			
  			if(error.status ==0){
  				this.toast.error("Can't connect to the server please try again")
  			}else{
  				this.toast.error("An error occured patient data could not be saved, please try again");
  			}
  			
  		}
  		);
  }


}
