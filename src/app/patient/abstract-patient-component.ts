import {ViewChild} from '@angular/core';
import { PatientFormComponent } from './patient-form/patient-form.component';
import MiniToast from '../../MiniToast';


declare var M:any
export class AbstractPatientComponent {
	@ViewChild(PatientFormComponent) patientForm: PatientFormComponent;
	loading = false;
	conError =false;
	serverError =false;
	M=M;
	toast = new MiniToast()
}
