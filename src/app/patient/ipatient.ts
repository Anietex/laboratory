export interface IPatient {
	id?:number;
 	surname: string;
    first_name: string;
    middle_name: string;
    age: number;
    sex: string;
    email: string;
    phone_no:string;
    requested_by: string;
    clinical_detail:string;
    exam_required : string;
    specimen: string;
    clinic: string;
   
}
