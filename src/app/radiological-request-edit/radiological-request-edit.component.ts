import { Component, OnInit,AfterViewInit } from '@angular/core';
import {radiologicalTests} from '../radiological-test';
import {IRadiologicalTest} from '../interfaces/iradiological-test';
import {IRadiologicalRequest} from '../interfaces/iradiological-request';
import {FormControl, FormGroup} from '@angular/forms';
import { RadiologicalRequestService } from '../services/radiological-request.service';
import MiniToast from '../../MiniToast';
import {ActivatedRoute} from '@angular/router';
import {ViewChild, ElementRef} from '@angular/core';
 
declare var M:any;
@Component({
  selector: 'app-radiological-request-edit',
  templateUrl: './radiological-request-edit.component.html',
  styles: []
})
export class RadiologicalRequestEditComponent implements OnInit {
	selectedTests: Array<IRadiologicalTest> = [];
    showBasicInfo = true;
    showLoading = false;
    request: IRadiologicalRequest;
    id:number;

    @ViewChild('investigationSelect') invSelect: ElementRef;


     basicForm = new FormGroup({
         surname: new FormControl(''),
         first_name: new FormControl(''),
         middle_name: new FormControl(''),
         sex: new FormControl(''),
         age: new FormControl(''),
         lmp: new FormControl(''),
         phone_no: new FormControl(''),
         address: new FormControl(''),
         occupation: new FormControl('')
     });


     investigation = new FormControl([]);


  constructor(private serviceProvider: RadiologicalRequestService, private route: ActivatedRoute) { }

  ngOnInit() {
        
        this.route.params.subscribe((param) => {
            this.id = param.id;
            this.getRequest()
        });

       
    }

    ngAfterViewInit(): void {
        M.AutoInit();
    }

    handleTestsSelected(event: Event){
       let elm :HTMLSelectElement = event.currentTarget as HTMLSelectElement;
        let selectedOption: Array<HTMLOptionElement> = [].filter.call(elm.options,(option)=>{
            return option.selected
        });

        let selectedInv: IRadiologicalTest[] = [];

        // this.selectedTests.length =0;
        [].map.call(selectedOption,(option)=>{
            let index= Number.parseInt(option.value);
            selectedInv.push(radiologicalTests[index]);
        });  

        this.showSelectedOptions(selectedInv);    
    }

    saveReport(){
   
        const toast = new MiniToast();
        this.showLoading = true;
        this.request = {id:this.id,...this.basicForm.value,investigations:this.selectedTests} as IRadiologicalRequest;
        this.serviceProvider.updateRequest(this.request).subscribe((data) =>  {
                    toast.success('Request was updated successfully');
                    this.showLoading = false;
                },
                (error) =>  {
                    toast.error('Request was not updated due to some system error please try again');
                    this.showLoading = false;
                })
    }


    selectTestOption(index: number){
       
    }

    nextPane(){
        this.showBasicInfo = false;
    }

    prevPane(){
        this.showBasicInfo = true;
    }

    getRequest(){
    	this.serviceProvider.getRequest(this.id).subscribe((data)=>{
    		this.patchForms(data as IRadiologicalRequest);
    	},
    	(error)=>{

    	}
    	)
    }

    patchForms(data: IRadiologicalRequest){
    	this.basicForm.patchValue({...data})
    	M.updateTextFields();

    	let savedInv: string[] = []

    	Object.keys(radiologicalTests).forEach((key)=>{
    		data.investigations.forEach(save=>{
    			if(save.name == radiologicalTests[key].name){
    				savedInv.push(key)
    			}
    		})
    	})

    	this.investigation.patchValue(savedInv);
    	// this.selectedTests = data.investigations;
    	
    	M.FormSelect.init(this.invSelect.nativeElement);

    	this.showSelectedOptions(data.investigations);

    }

    showSelectedOptions(options: IRadiologicalTest[]){

    	console.log(options);

    	this.selectedTests = this.selectedTests.filter((test)=>{
    		for(const option of options){
    			if(option.name == test.name){
    				return true;
    			}
    			
    		}
    		return false;
    	});





    	options = options.filter((option)=>{
    		for(const test of this.selectedTests){
    			if(test.name === option.name){
    				return false;
    			}
    		}
    		return true;
    	});

    	


    	console.log(options);


        [].map.call(options,(option)=>{
            this.selectedTests.push(option);
        });
    }






}
