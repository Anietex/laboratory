import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {radiologicalTests} from '../radiological-test';
import { RadiologicalRequestService } from '../services/radiological-request.service';
import {IRadiologicalRequest} from '../interfaces/iradiological-request';

@Component({
  selector: 'app-radiological-request-view',
  templateUrl: './radiological-request-view.component.html',
  styleUrls: ['./radiological-request-view.component.scss']
})
export class RadiologicalRequestViewComponent implements OnInit {

	id:number ;
	request: IRadiologicalRequest;
	loading = false;
	errorOccurred= false;
	investigations = []
  constructor(private serviceProvider: RadiologicalRequestService, private route: ActivatedRoute) { }

  ngOnInit() {
        
        this.route.params.subscribe((param) => {
            this.id = param.id;
            this.getRequest()
        });

       
    }



    getRequest(){
    	this.loading = true;
    	this.errorOccurred = false;
    	this.serviceProvider.getRequest(this.id).subscribe((data)=>{
    		this.loading = false;
    		this.parseRequest(data as IRadiologicalRequest)
    	},
    	(err)=>{
    		this.loading = false;
    		this.errorOccurred = true;
    	})
    }


    parseRequest(request: IRadiologicalRequest){
    	
    	let saveInv = radiologicalTests;
    	let invs = Object.keys(saveInv);
    	for(const inv of invs){
    		for(const invest of request.investigations){
    			 if(invest.name === saveInv[inv].name){
    			 	saveInv[inv] = invest;
    			 }
    		}
    	}

    	this.request = request;
    	Object.keys(saveInv).forEach((index)=>{
    		this.investigations.push(saveInv[index])
    	})
    
    }



    wasSelected(invest): boolean{
    	return this.request.investigations.find((inv)=>{
    		return invest.name == inv.name 
    	}) != undefined;
    }


    printPage(){
    	window.print();
    }

}
