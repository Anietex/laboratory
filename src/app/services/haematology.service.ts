import {Inject, Injectable} from '@angular/core';
import {IHaematologyReport} from '../interfaces/ihaematology-report';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class HaematologyService {

    constructor(@Inject('API_URL') private apiUrl: string,private http: HttpClient) { }

    public getReports(){
        return this.http.get(this.apiUrl+'haematology-reports');
    }

    createReport(report: IHaematologyReport){
        const formData = new FormData();
        let fields :string[] = Object.keys(report);
        for(const field of fields){
            if(field=='id') continue;
            formData.append(field,report[field]);
        }

      return  this.http.post(this.apiUrl+'haematology-reports',formData)
    }

    getReport(id: number){
        return this.http.get(this.apiUrl+'haematology-reports/'+id)
    }

    deleteReport(id:number){
           const formData = new FormData();
           formData.append('_method',"DELETE");
           return this.http.post(this.apiUrl+'haematology-reports/'+id,formData);
    }


    updateReport(report:IHaematologyReport){
        const formData = new FormData();
        formData.append('_method','PATCH');
        let fields :string[] = Object.keys(report);
        for(const field of fields){
            if(field=='id') continue;
            formData.append(field,report[field]);
        }

        return  this.http.post(this.apiUrl+'haematology-reports/'+report.id,formData)
    }


}
