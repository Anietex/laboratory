import { TestBed, inject } from '@angular/core/testing';

import { MedicalMicrobiologyService } from './medical-microbiology.service';

describe('MedicalMicrobiologyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MedicalMicrobiologyService]
    });
  });

  it('should be created', inject([MedicalMicrobiologyService], (service: MedicalMicrobiologyService) => {
    expect(service).toBeTruthy();
  }));
});
