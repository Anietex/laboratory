import { Injectable, Inject } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IRadiologicalRequest } from '../interfaces/iradiological-request';


@Injectable({
  providedIn: 'root'
})
export class RadiologicalRequestService {

  constructor(@Inject('API_URL') private apiUrl: string,private http: HttpClient) { }



  createRequest(request: IRadiologicalRequest){
  	const formData = new FormData();
  	formData.append('request_data',JSON.stringify(request));
  	return this.http.post(this.apiUrl+'radiological-request',formData);
  }


  getRequests(){
  		return this.http.get(this.apiUrl+'radiological-request');
  }



  getRequest(id:number){
  	return this.http.get(this.apiUrl+'radiological-request/'+id);
  }


  updateRequest(request: IRadiologicalRequest){
    const formData = new FormData();
    formData.append('_method','PATCH');
    formData.append('request_data',JSON.stringify(request));
    return this.http.post(this.apiUrl+'radiological-request/'+request.id,formData);
  }

  deleteRequest(id:number){
     const formData = new FormData();
     formData.append('_method',"DELETE");
     return this.http.post(this.apiUrl+'radiological-request/'+id,formData);
  }


}
