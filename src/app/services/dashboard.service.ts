
import { Injectable , Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(@Inject('API_URL') private apiUrl: string,private http: HttpClient) { }


  getSummary(){
  	return this.http.get(this.apiUrl+'dashboard-summary');
  }
  
}
