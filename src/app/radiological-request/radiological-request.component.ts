import { Component, OnInit,AfterViewInit } from '@angular/core';
import {radiologicalTests} from '../radiological-test';
import {IRadiologicalTest} from '../interfaces/iradiological-test';
import {IRadiologicalRequest} from '../interfaces/iradiological-request';
import {FormControl, FormGroup} from '@angular/forms';
import { RadiologicalRequestService } from '../services/radiological-request.service';
import MiniToast from '../../MiniToast';

declare var M:any;
@Component({
    selector: 'app-radiological-request',
    templateUrl: './radiological-request.component.html',
    styleUrls: ['./radiological-request.component.scss']
})
export class RadiologicalRequestComponent implements OnInit,AfterViewInit {
    selectedTests: Array<IRadiologicalTest> = [];
    showBasicInfo = true;
    showLoading = false;
    request: IRadiologicalRequest;

     basicForm = new FormGroup({
         surname: new FormControl(''),
         first_name: new FormControl(''),
         middle_name: new FormControl(''),
         sex: new FormControl(''),
         age: new FormControl(''),
         lmp: new FormControl(''),
         phone_no: new FormControl(''),
         address: new FormControl(''),
         occupation: new FormControl('')
     });

    constructor(private serviceProvider: RadiologicalRequestService) { }

    ngOnInit() {
    }

    ngAfterViewInit(): void {
        M.AutoInit();
    }

    handleTestsSelected(event: Event){
        let elm :HTMLSelectElement = event.currentTarget as HTMLSelectElement;
        let selectedOption: Array<HTMLOptionElement> = [].filter.call(elm.options,(option)=>{
            return option.selected
        });
        this.selectedTests.length =0;
        [].map.call(selectedOption,(option)=>{
            let index= Number.parseInt(option.value);
            this.selectedTests.push(radiologicalTests[index]);
        });

    }

    saveReport(){
         const toast = new MiniToast();
         this.showLoading = true;
        this.request = {...this.basicForm.value,investigations:this.selectedTests} as IRadiologicalRequest;
        this.serviceProvider.createRequest(this.request).subscribe((data) =>  {
                    toast.success('Request was saved successfully');
                    this.showLoading = false;
                    this.resetForm();
                },
                (error) =>  {
                    toast.error('Request was not save due to some system error please try again');
                    this.showLoading = false;
                })
    }


    selectTestOption(index: number){
        console.log(index)
    }

    nextPane(){
        this.showBasicInfo = false;
    }

    prevPane(){
        this.showBasicInfo = true;
    }

    resetForm(){

    }


}
