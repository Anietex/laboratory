import { Injectable, Inject,EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import  {   tokenNotExpired, JwtHelper   } from 'angular2-jwt';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
	jwtHelper: JwtHelper = new JwtHelper();

  onLogIn = new EventEmitter<any>();

  constructor(@Inject('API_URL') private appUrl: string, private http: HttpClient ) { }

  login(formData){
  	return this.http.post(this.appUrl+'login', formData);
  }
 

  setToken(token){
  	localStorage.setItem('auth_token',token);

  }

  

  getToken(){
  	return localStorage.getItem('auth_token');
  }

  setUser(user){
    localStorage.setItem('auth_user',JSON.stringify(user));
    this.onLogIn.emit();
  }

  getUser(){
    return  JSON.parse(localStorage.getItem('auth_user'));
  }


  isLogin(){
    if(this.getToken()){
      return ! this.jwtHelper.isTokenExpired(this.getToken())
    }else{
      return  false;
    }
  	
  }

  isAdmin(){
    return this.getUser()?this.getUser().admin == 1:false;
  }


  logout(){
    return  this.http.get(this.appUrl+'logout');
  }


  clearAuth(){
    
    localStorage.removeItem('auth_token');
    localStorage.removeItem('auth_user');

  }


}
