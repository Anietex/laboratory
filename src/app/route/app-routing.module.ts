import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HaematologyCreateReport} from '../haematology/create-report/create-report.component';
import { CreateChemicalPathologyComponent} from '../chemical-pathology/create-chemical-pathology/create-chemical-pathology.component';
import { CreateMedicalMicrobiologyComponent} from '../medical-microbiology/create-medical-microbiology/create-medical-microbiology.component';
import { RadiologicalRequestComponent} from '../radiological-request/radiological-request.component';
import { HaematologyReportsComponent} from '../haematology/haemtology-reports/haematology-reports.component';
import { HaematologyReportViewComponent} from '../haematology/haematology-report-view/haematology-report-view.component';
// import { HaematologyEditComponent } from '../haematology-edit/haematology-edit.component';
import { ChemicalPathologyReportsComponent } from '../chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component';
import { ChemicalPathologyEditComponent } from '../chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component';
import { ChemicalPathologyViewComponent } from '../chemical-pathology/chemical-pathology-view/chemical-pathology-view.component';
import { MedicalMicrobiologyReportsComponent } from '../medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component';
import { EditMedicalMicrobiologyComponent } from '../medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component';
import { MedicalMicrobiologyViewComponent } from '../medical-microbiology/medical-microbiology-view/medical-microbiology-view.component';
import { RadiologicalRequestListComponent  } from '../radiological-request-list/radiological-request-list.component';
import { RadiologicalRequestEditComponent  } from '../radiological-request-edit/radiological-request-edit.component';
import { RadiologicalRequestViewComponent  } from '../radiological-request-view/radiological-request-view.component';
import { CreatePatientComponent } from '../patient/create-patient/create-patient.component';
import { PatientListComponent } from '../patient/patient-list/patient-list.component';
import { EditPatientComponent } from '../patient/edit-patient/edit-patient.component';
import { EditReportComponent as HaematologyEditReport } from '../haematology/edit-report/edit-report.component';
import {SettingsComponent } from '../settings/settings.component';
import { ManageLabsComponent } from '../manage-labs/manage-labs.component';
import { LoginComponent  } from '../login/login.component';
import  {AuthGuard } from '../Auth/auth.guard';
import {PatientLogComponent } from '../patient/patient-log/patient-log.component';

const routes:  Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children:[

                    {
                        path:'',
                        pathMatch:'full',
                        redirectTo:'dashboard'
                    },
                   {
                        path: 'dashboard',
                        component: DashboardComponent
                    },

                    {
                        path: 'haematology/new',
                        component: HaematologyCreateReport,
                    },
                    {
                        path: 'chemical-pathology/new',
                        component: CreateChemicalPathologyComponent
                    },
                    {
                        path: 'medical-microbiology-and-parasitology/new',
                        component: CreateMedicalMicrobiologyComponent
                    },
                    {
                        path: 'radiological-request/new',
                        component: RadiologicalRequestComponent,
                    },
                    {
                        path: 'haematology/reports',
                        component: HaematologyReportsComponent
                    },
                    {
                        path: 'haematology/reports/:id',
                        component: HaematologyReportViewComponent,
                    },
                    {
                      path: 'haematology/edit/:id',
                      component: HaematologyEditReport
                    },
                    {
                        path:'chemical-pathology/reports',
                        component:ChemicalPathologyReportsComponent
                    },
                    {
                        path:'chemical-pathology/edit/:id',
                        component:ChemicalPathologyEditComponent
                    },
                    {
                         path:'chemical-pathology/reports/:id',
                        component:ChemicalPathologyViewComponent
                    },
                    {
                        path:'medical-microbiology-and-parasitology/reports',
                        component:MedicalMicrobiologyReportsComponent
                    },
                    {
                        path:'medical-microbiology-and-parasitology/edit/:id',
                        component:EditMedicalMicrobiologyComponent
                    },
                     {
                        path:'medical-microbiology-and-parasitology/reports/:id',
                        component:MedicalMicrobiologyViewComponent
                    },
                    {
                        path:'radiological-request/requests',
                        component:RadiologicalRequestListComponent
                    },

                     {
                        path:'radiological-request/edit/:id',
                        component:RadiologicalRequestEditComponent
                    },
                    {
                        path:'radiological-request/requests/:id',
                        component:RadiologicalRequestViewComponent
                    },
                    {
                        path:'patient/new',
                        component:CreatePatientComponent
                    },
                    {
                        path:'patient/edit/:id',
                        component:EditPatientComponent
                    },
                    {
                        path:'patients',
                        component:PatientListComponent
                    },
                    {
                        path:'patient/log/:id',
                        component:PatientLogComponent
                    },
                    {
                        path:'settings',
                        component:SettingsComponent
                    },
                    {
                        path:'manage-labs',
                        component: ManageLabsComponent
                    }
                ]
            },
    {
        path:'login',
        component:LoginComponent
    },
    
    
];

@NgModule({
    imports:  [RouterModule.forRoot(routes)],
    exports:  [RouterModule]
})
class AppRoutingModule { }
export {AppRoutingModule};
