import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import {IHaematologyReport} from '../ihaematology-report';
import {HaematologyService} from '../haematology.service';
import {ActivatedRoute} from '@angular/router';
import MiniToast from '../../../MiniToast';
import { AbstractTableComponent } from '../../shared/abstract-table-component';

  

declare var roar:  any;
@Component({
    selector: 'app-haemtology-reports',
    templateUrl: './haemtology-reports.component.html',
    styleUrls: ['./haemtology-reports.component.scss']
})
export class HaematologyReportsComponent extends AbstractTableComponent implements  OnInit {
    public loading = false;
    public errorOccurred=false;
    public deleting = false;


    columnsToDisplay = ['id','patient_name','exam_required','specimen','createdOn','view','edit','delete'];

    constructor(private serviceProvider:  HaematologyService) { 
        super();
        this.getReports = this.getReports.bind(this);

    }


    public reports: IHaematologyReport[];

    ngOnInit() {
        this.getReports();


    }


    public getReports(){
        this.loading = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReports().subscribe((data)=>{
            this.loading = false;
                this.reports = data as IHaematologyReport[];

                this.matDatasource.data = this.reports;

                this.matDatasource.paginator = this.paginator;
            },
            (err)=>{
                this.loading = false;
                if(err.status ==0){
                    this.conError = true
                }else{
                     this.serverError = true;
                }               
            })
    }


    public deleteReport(id,index){
         const toast = new MiniToast();
        
       var options = {
       cancel: true,
       cancelText: 'Cancel',
       cancelCallBack: function (event) {},
       confirm: true,
        confirmText: 'Delete',
        confirmCallBack: (event)=> {
            this.deleting = true;
            this.serviceProvider.deleteReport(id).subscribe((res)=>{
                toast.success('Report was deleted succesfully');
                this.deleting = false;
                this.reports.splice(index,1);
                this.matDatasource.data = this.reports;
                this.matDatasource.paginator = this.paginator;
            },
            (err)=>{
                toast.error("Report could not be deleted at the moment please try again")
            }
            )
           }
        
        }

        roar('Delete Report', 'Do you realy want to  <strong>Delete </strong> this report', options);
    }


   



}
