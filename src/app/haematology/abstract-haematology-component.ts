import {ReportFormComponent } from './report-form/report-form.component';
import  {ViewChild } from '@angular/core';
import MiniToast from '../../MiniToast';

export class AbstractHaematologyComponent {

	@ViewChild(ReportFormComponent) reportForm;
	saving = false;
	toast = new MiniToast();
	
	public resetForm(){
		this.reportForm.reset();
	}
}
