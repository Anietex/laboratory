import { Component, OnInit,AfterViewInit, EventEmitter,Output,Input, AfterViewChecked,OnChanges } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AbstractCreateReport } from '../../shared/abstract-create-report';
import { Router, ActivatedRoute } from '@angular/router';
import {PatientService } from '../../patient/patient.service';
import {IPatient } from '../../patient/ipatient';
import {AbstractPaneComponent } from '../../shared/abstract-pane-component';
import {IHaematologyReport } from '../ihaematology-report';
import  Minitoast from '../../../MiniToast';
import {AbstractReportForm } from '../../shared/abstract-report-form';
declare let M:any;
   
@Component({
  selector: 'hem-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.scss']
})

export class ReportFormComponent extends AbstractReportForm implements OnInit,AfterViewChecked,OnChanges {

 	constructor(protected router: Router, 
            protected patientService: PatientService, 
            protected activeRoute: ActivatedRoute){
 		super(router,patientService,activeRoute);
 		
 	}

 	

 	@Input() report: IHaematologyReport;
 	@Output() submit =  new EventEmitter<IHaematologyReport>();
 	@Input() saveBtnText = "Save Report";
  @Input() saving : boolean;


 	patients:IPatient[];


 	hemForm:  FormGroup = new FormGroup({
 		patient_id: new FormControl(''),
 		requested_by: new FormControl(),
 		clinical_detail: new FormControl(),
 		exam_required: new FormControl(),
 		specimen: new FormControl(),
 		clinic: new FormControl(),
        hb: new FormControl(),
        pcv: new FormControl(),
        rbc: new FormControl(),
        mcv: new FormControl(),
        mch: new FormControl(),
        mchc: new FormControl(),
        wbc:  new FormControl(),
        platelets: new FormControl(),
        retics: new FormControl(),
        esr: new FormControl(),
        nuet:  new FormControl(''),
        lymp:  new FormControl(''),
        mono:  new FormControl(''),
        eosino:  new FormControl(''),
        baso:  new FormControl(''),
        blast:  new FormControl(''),
        band: new FormControl(''),
        promvelo:  new FormControl(''),
        myelo:  new FormControl(''),
        matemye:  new FormControl(''),
        normod:  new FormControl(''),
        bleeding_time:  new FormControl(''),
        clotting_time:  new FormControl(''),
        prothrobin_time:  new FormControl(''),
        partial_prothrobin_time:  new FormControl(''),
        blood_group:  new FormControl(''),
        hb_genotype:  new FormControl(''),
        cross_matching:  new FormControl(''),
        film_report: new FormControl(''),
        lab_scientist:  new FormControl(''),
        screening: new FormControl(''),
        quantitative:  new FormControl(''),
    });

  

  

  ngOnChanges(){
  	if(this.report){
  		this.hemForm.patchValue({...this.report})
  	}
  }

 

	showPane(){
		if(this.activePane>4){
			this.showNextBtn = false;
			this.showSave = true;
		}else{
			this.showNextBtn = true;
			this.showSave = false;

		}
		 if(this.activePane >0 ){
			this.showPrevBtn = true;
		}else{
			this.showPrevBtn = false;
		}
	}


	handleSubmit(){
		let patient_id = Number.parseInt(this.hemForm.value.patient_id)
		if(isNaN(patient_id)){
			new Minitoast().warning("You have not seleted any patient");
			this.activePane = 0;
			this.showPane();
		}else{
			this.submit.emit(this.hemForm.value)
		}
		
	}


	reset(){
		this.hemForm.reset();
		setTimeout(()=>{this.initMaterial(); M.updateTextFields();},200);
		this.activePane = 0;
		this.showPane();
	}


}
