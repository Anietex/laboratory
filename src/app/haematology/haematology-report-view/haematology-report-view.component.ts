import { Component, OnInit } from '@angular/core';
import {HaematologyService} from '../haematology.service';
import {ActivatedRoute} from '@angular/router';
import {IHaematologyReport} from '../ihaematology-report';

 
@Component({
  selector: 'app-haematology-report-view',
  templateUrl: './haematology-report-view.component.html',
  styleUrls: ['./haematology-report-view.component.scss']
})
export class HaematologyReportViewComponent implements OnInit {

    public report:IHaematologyReport;
    loadingReport = false;
    conError = false;
    serverError = false;
    id = null;

  constructor(private route: ActivatedRoute,private serviceProvider: HaematologyService) {
      this.getReport = this.getReport.bind(this)
   }
 
  ngOnInit() {
      this.route.params.subscribe((param)=>{
        this.id = param.id;
        this.getReport()
      });



      
  }

  getReport(){
       this.loadingReport = true;
       this.conError = false;
       this.serverError = false;


        this.serviceProvider.getReport(this.id).subscribe((report)=>{
             this.loadingReport = false;
             const keys = Object.keys(report);

             for(const key of keys){

                 if( report[key] === null  || report[key] == 'null' ){
                     report[key]='';
                 }else if( report[key]===''){
                     report[key]='';
                 }
             }
            this.report = report as IHaematologyReport;
            

         },(err)=>{
            this.loadingReport = false;
           if(err.status == 0){
               this.conError = true;
           }else{
             this.serverError = true;
           }
         }
         );

  }


  

}
