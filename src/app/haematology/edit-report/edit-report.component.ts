import { Component, OnInit } from '@angular/core';
import { AbstractHaematologyComponent } from '../abstract-haematology-component';
import { HaematologyService } from '../haematology.service';
import  {IHaematologyReport } from '../ihaematology-report';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-report',
  templateUrl: './edit-report.component.html',
  styleUrls: ['./edit-report.component.scss']
})

export class EditReportComponent extends AbstractHaematologyComponent  implements OnInit {

  constructor(private serviceProvider: HaematologyService, private route: ActivatedRoute ) {
  	super();
  		this.getReport = this.getReport.bind(this)
  	 }
  id= null;
  report :  IHaematologyReport;
  conError = false;
  serverError = false;
  loadingReport = false;
 
  ngOnInit() {
  	this.route.params.subscribe((params)=>{
  		this.id = params.id;
  		this.getReport();
  	})
  }


   onSubmit(report: IHaematologyReport){
  	this.saving = true;
    report.id = this.id;
    this.report = report;


  	this.serviceProvider.updateReport(report).subscribe(
  		(data)=>{
  			this.saving = false;
  			this.toast.success('Report was updated successfully')
  		},
  		(error)=>{
  			this.saving = false;
  			if(error.status == 0){
  				this.toast.error("Can't connect to server please try again");
  			}else{
  				this.toast.error("An error occurred report could not be updated");
  			}
  		}
  		)
  	
  }


  getReport(){
  	this.loadingReport = true;
  	this.conError = false;
  	this.serverError = false;
  	this.serviceProvider.getReport(this.id).subscribe((report)=>{
      console.log(report);
      const keys = Object.keys(report);

             for(const key of keys){
                 if( report[key] === null  || report[key] == 'null' ){
                     report[key]='';
                 }else if( report[key]===''){
                     report[key]='';
                 }
             }
  		this.report = report as IHaematologyReport;
  		this.loadingReport = false;
  	},
  	(err)=>{
  		this.loadingReport = false;
  		if(err.status == 0){
  			this.conError = true;
  		}else{
  			this.serverError = true;
  		}
  	}
  	)
  }

}
