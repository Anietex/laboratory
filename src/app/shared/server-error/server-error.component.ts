import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'server-error',
  templateUrl: './server-error.component.html',
  styles: []
})
export class ServerErrorComponent implements OnInit {
	@Input() retryCb;
	@Input() show:boolean;
  constructor() { }

  ngOnInit() {
  }

  retry(){
  	if(this.retryCb)
  		this.retryCb()

  }

}
 