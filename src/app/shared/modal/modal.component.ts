import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormControl } from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

	@ViewChild('modal') modal: ElementRef;

	sms = new FormControl('');
	email = new FormControl('');
	continueCb: Function;
	cancelCb : Function;

  constructor() { }

  ngOnInit() {

  }


  public openModal(continueCb?:Function){
  	this.continueCb = continueCb
  	this.modal.nativeElement.style.display = 'block';
  }


  public closeModal(){
  	this.modal.nativeElement.style.display = 'none';
  }


  public onContinue(){
   	this.closeModal();
   		let options = {
  		sms:this.sms.value?true:false,
  		email:this.email.value?true:false
  	}


  	if(this.continueCb){
   			this.continueCb(options);
   		}

   }


 public  onCancel(){
   		this.closeModal();

   		if(this.continueCb){
   			this.continueCb();
   		}
   		
   }

}
