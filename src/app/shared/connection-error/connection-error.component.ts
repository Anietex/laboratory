import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'connection-error',
  templateUrl: './connection-error.component.html',
  styles: []
})
export class ConnectionErrorComponent implements OnInit {

	@Input() retryCb;
	@Input() show:boolean;
  constructor() { }

  ngOnInit() {

  }


  retry(){
  	if(this.retryCb)
  		this.retryCb()

  }

}
