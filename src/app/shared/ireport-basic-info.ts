import {IPatient} from '../patient/ipatient';

export interface IReportBasicInfo {
	id: number;
    lab_id?:number;
	patient_id: number;
	patient?:IPatient;
 	requested_by:string,
 	clinical_detail: string,
 	exam_required: string,
 	specimen: string,
 	clinic: string,
}

