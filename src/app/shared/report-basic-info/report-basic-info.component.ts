import {
    Component,
    OnInit,
    OnDestroy,
    Output,
    EventEmitter,
    OnChanges,
    SimpleChange,
    SimpleChanges,
    AfterViewChecked,
    AfterViewInit, Input
} from '@angular/core';

import { FormGroup, FormControl } from '@angular/forms';
import { IReportBasicInformation } from '../../interfaces/ireport-basic-information';

declare var M : any;
@Component({
  selector:  'app-report-basic-info',
  templateUrl:  './report-basic-info.component.html',
  styleUrls:  ['./report-basic-info.component.scss']
})
export class ReportBasicInfoComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges, AfterViewChecked {
   surname:  string;
   firstName:  string;
   middleName:  string;
   age:  number;
   sex:  string;
   date:  string;
   time:  string;
   requestedBy:  string;
   email:  string;
   examRequired:  string;
   specimen:  string;
   clinic:  string;
   lab_no:string;
   clinical_detail:string;

   @Input() storage: string;


   basicInfoForm = new FormGroup({
       lab_no: new FormControl(''),
       surname:  new FormControl(''),
       first_name:  new FormControl(''),
       middle_name:  new FormControl(''),
       age:  new FormControl(''),
       sex:  new FormControl(''),
       date:  new FormControl(''),
       time:  new FormControl(''),
       requested_by:  new FormControl(''),
       email:  new FormControl(''),
       exam_required:  new FormControl(''),
       specimen:  new FormControl(''),
       clinic:  new FormControl(''),
       clinical_detail: new FormControl('')
   });




   @Output() onHidden = new EventEmitter<IReportBasicInformation>();
   @Input() basicInfo:  IReportBasicInformation;
   constructor() { }


  ngOnInit() {
       this.basicInfoForm.valueChanges.subscribe(value => {
           if (this.basicInfo == null) {
               localStorage.setItem(this.storage, JSON.stringify(value));
           }
       });

  }
    ngAfterViewInit(){
        this.getStorageValue();
        this.initMaterial();
    }

  ngOnChanges() {
    this.getStorageValue();

  }

  ngOnDestroy() {
      const basicInfo:  IReportBasicInformation = this.basicInfoForm.value;
        if (!this.basicInfo) {
            localStorage.setItem(this.storage, JSON.stringify(basicInfo));
          }

      this.onHidden.emit(basicInfo);
  }

    ngAfterViewChecked() {
        M.updateTextFields();
    }

  private getStorageValue() {

      if (this.basicInfo != null) {

          this.basicInfoForm.patchValue({...this.basicInfo});

      } else {
        const  info:  IReportBasicInformation = JSON.parse(localStorage.getItem(this.storage));
        if (info != null) {
          this.basicInfoForm.patchValue({...info});

        }
      }

  }

  private initMaterial() {
      M.AutoInit();
      const elems = document.querySelectorAll('.datepicker');
      const  that = this;
      M.Datepicker.init(elems, {
          container :  document.body,
          format:  'mm/dd/yyyy',
          onClose:  function () {
              const date = this.date as Date;
              if (date) {
                  that.basicInfoForm.patchValue({date:  date.toLocaleDateString()})
              }

          }
      });

      const timeElm = document.querySelectorAll('.timepicker');
      M.Timepicker.init(timeElm,{
          container:  'body',
          onCloseEnd:  function () {
              that.basicInfoForm.patchValue({time: this.time});
          }
      });
  }


}
