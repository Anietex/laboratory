import { Input,OnInit,AfterViewInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { IPatient } from '../patient/ipatient';
import { PatientService } from '../patient/patient.service';
import { AbstractPaneComponent } from './abstract-pane-component';

declare let M:any;

export abstract class AbstractReportForm extends AbstractPaneComponent implements OnInit,AfterViewInit {

	@Input() saveBtnText = "Save Report";
	@Input() saving : boolean;

	patients:IPatient[];
	loadingPatients = false;
 	errorOccurred = false;

	constructor(protected router: Router,
				protected patientService: PatientService,
				protected activeRoute: ActivatedRoute ){
		super();
		this.reset =  this.reset.bind(this);

		console.log(activeRoute.routeConfig.path);
	}

	abstract reset();

	ngOnInit() {
  	this.getPatients();

 	}


 	protected getPatients(){
		this.loadingPatients = true;
		this.errorOccurred = false;
		setTimeout(()=>{this.initMaterial()},200);
		this.patientService.getPatients().subscribe((patients)=>{
			this.patients = patients as IPatient[];
			this.loadingPatients = false;
			setTimeout(()=>{this.initMaterial()},200);
		},
		(error)=>{
			this.loadingPatients = false;
			this.errorOccurred = true;
			setTimeout(()=>{this.initMaterial()},200);
		});
	}



	 protected initMaterial(){
  	  let elems = document.querySelectorAll('select');
      M.FormSelect.init(elems);
   }



   ngAfterViewChecked(){
  	 M.updateTextFields();
  }

  ngAfterViewInit(){
  	this.initMaterial();
  }


 

	onPatientSelected(event){
		if(event.target.value=='new'){
			this.router.navigateByUrl('/patient/new?r=/'+this.activeRoute.routeConfig.path);
		}
		else if(event.target.value=='retry'){

			this.getPatients();

		}
	}




}
