export abstract class AbstractPaneComponent {



	public activePane = 0;
	public  showNextBtn: boolean = true;
    public  showPrevBtn: boolean = false;
    public  showSave: boolean;

    abstract showPane(): void;
	nextPane() {
        this.activePane++;
        this.showPane();
    }

    prevPane() {
        this.activePane--;
        this.showPane();
    }
}
