import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-linear-loading',
  templateUrl: './linear-loading.component.html',
  styles: [`.progress{margin-bottom:0}`]
})
export class LinearLoadingComponent implements OnInit {
	@Input() showLoading:boolean;

  constructor() { }

  ngOnInit() {
  }

}
