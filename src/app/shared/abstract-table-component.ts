import {MatPaginator, MatTableDataSource, MatTable,MatSort} from '@angular/material';
import {ViewChild } from '@angular/core';
declare var roar:  any;
import MiniToast from '../../MiniToast';

export abstract class AbstractTableComponent {

	   @ViewChild(MatPaginator) paginator: MatPaginator;
     @ViewChild(MatSort) sort: MatSort;
     conError = false;
     serverError = false;
     
     matDatasource = new MatTableDataSource();



    @ViewChild(MatSort) set content(content: MatSort) {
      this.sort = content;
    if (this.sort){
         this.matDatasource.sort = this.sort;

     }
   }


   @ViewChild(MatPaginator) set pager(content: MatPaginator) {
      this.paginator = content;
    if (this.paginator){
         this.matDatasource.paginator = this.paginator;

     }
   }

   roar = roar;
   toast = new MiniToast();


    filterTable(searchText: string){
        this.matDatasource.filter = searchText.trim().toLowerCase();
    }

}
