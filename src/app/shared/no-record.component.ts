import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'no-record',
  template: `
    <div class="card-panel" *ngIf="show">
      <div class="center-align">
        <span><ng-content></ng-content></span>
      </div>
    </div>
  `,
  styles: [`
  span{
    font-size:1.2em;
  }

  .card-panel{
    padding:60px 0;
  }
  `
  ]
})
export class NoRecordComponent implements OnInit {
  @Input() show:boolean;
  constructor() { }

  ngOnInit() {
  }

}
