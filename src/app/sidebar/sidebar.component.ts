import {Component, OnInit, AfterViewChecked, AfterViewInit} from '@angular/core';
import * as moment from 'moment';
import { SettingService } from '../settings/setting.service';
import { AuthService } from '../Auth/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, AfterViewInit {
  hour:any;
  minute:any;
  second:any;
  day:any;
  month:any;
  date:any;
  year:any;

  isAdmin = false;

  labName = '';
  subName = '';

  constructor(private setting: SettingService, private authService: AuthService) {
    this.authService.onLogIn.subscribe(()=>{
        alert('log in');
    });


  
   }

   updateAdmin(){
     if(this.authService){
        this.isAdmin = this.authService.isAdmin();
     }
     
   }

  ngOnInit() {
    this.showTime();
    this.getSetting();
    
    this.setting.onSettingsUpdated.subscribe(()=>{
       this.getSetting();
    });


  } 


  getSetting(){
    let set = this.setting.getLocalSettings();
    if(set){
       this.labName = set.lab_name;
       this.subName = set.sub_name;
    }

   
  }


  ngAfterViewInit() {
      const drops: NodeList = document.querySelectorAll('.dropdown-toggle');
      const that: SidebarComponent = this;
      [].map.call(drops, function (dropdown) {
          dropdown.onclick = function (event: Event) {
                event.preventDefault();
              const trigger: HTMLElement  = event.currentTarget as HTMLElement;
              const target: HTMLElement = document.querySelector('#' +trigger.dataset.target);
              if (target.style.display === 'block') {
                  trigger.classList.remove('active');
                  target.style.display = 'none';
                  trigger.lastElementChild.textContent = 'keyboard_arrow_right';
              } else {
                  that.hideDropdowns(drops);
                  target.style.display = 'block';
                  trigger.classList.add('active');
                  trigger.lastElementChild.textContent = 'keyboard_arrow_down';
              }
          };


      });
  }
  private hideDropdowns(drops: NodeList): void {
      [].map.call(drops, function (drop: HTMLElement) {
          const target: HTMLElement = document.querySelector('#' + drop.dataset.target);
          if (target.style.display === 'block') {
              drop.classList.remove('active');
              drop.lastElementChild.textContent = 'keyboard_arrow_right';
              target.style.display = 'none';
          }
      });
  }



  showTime(){
      setInterval(()=>{
        this.updateAdmin();
        this.second = moment().format('ss');
        this.minute = moment().format('mm');
        this.hour = moment().subtract({hour:1}).format('hh');
        this.day = moment().format('dddd');
        this.month = moment().format('MMMM');
        this.date = moment().format('Do');
        this.year = moment().format('YYYY')
      },500);
  }
}
