export interface IReportBasicInformation {
    requested_by: string;
    exam_required: string;
    specimen: string;
    clinic: string;
    clinical_detail:string;
    lab_id:string;
}
