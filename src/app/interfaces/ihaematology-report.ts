import {IReportBasicInformation} from './ireport-basic-information';
import {FormControl} from '@angular/forms';

export interface IHaematologyReport extends IReportBasicInformation{
    id: number;
    hb:string,
    pcv:string,
    rbc:string,
    mcv:string,
    mch:string,
    mchc:string,
    wbc: string,
    platelets:string,
    retics:string,
    esr:string,
    nuet: string;
    lymp: string;
    mono: string;
    eosino: string;
    baso: string;
    blast: string;
    promvelo: string;
    myelo: string;
    matemye: string;
    normod: string;
    bleeding_time: string;
    clotting_time: string;
    prothrobin_time: string;
    partial_prothrobin_time: string;
    blood_group: string;
    hb_genotype: string;
    cross_matching: string;
    film_report:string;
    lab_scientist: string;
    screening:string;
    quantitative: string;


}
