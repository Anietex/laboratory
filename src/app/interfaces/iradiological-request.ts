import {IRadiologicalTest } from './iradiological-test';

export interface IRadiologicalRequest {
	id:number;
	surname:string;
	first_name:string;
	middle_name:string;
	age:number;
	sex:string;
	lmp:string;
	phone_no:string;
	address:string;
	occupation:string;
	investigations:IRadiologicalTest[];
}
