import {RadiologicalTestOption} from '../classes/radiological-test-option';
import {IRadiologicalTestOption} from './iradiological-test-option';

export interface IRadiologicalTest {
    name: string;
    options: Array<IRadiologicalTestOption>;
}
