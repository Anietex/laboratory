import {IRadiologicalTest} from './interfaces/iradiological-test';
import {RadiologicalTest} from './classes/radiological-test';

type TestsObjects={
    [key:number]:IRadiologicalTest;
}




let radiologicalTests: TestsObjects = {
    1: {
        name: "Pap smear",
        options: []
    },
    2: {
        name: "Mammography",
        options: []
    },
    3: {
        name: "Chest",
        options: [
            {
                value: "PA",
                selected: false
            },
            {
                value: "Lat",
                selected: false,
            },
            {
                value: "Apical view",
                selected: false
            }
        ]
    },
    4: {
        name: "Cervical Spin",
        options: []
    },
    5: {
        name: "Soft Tissue Neck",
        options: []
    },
    6: {
        name: "Thoracolumbar Spine",
        options: []
    },
    7: {
        name: "Lumbosacral Spine",
        options: [
            {
                value: "AP",
                selected: false
            },
            {
                value: "Lat",
                selected: false,
            },
            {
                value: "Oblique",
                selected: false,
            }
        ]
    },
    8:{
        name:"Clavicle",
        options:[]
    },
    9:{
        name:"Shoulder join",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            },
            {
                value:"Int. Rot.",
                selected:false,
            },
            {
                value:"Ext. Rot.",
                selected:false
            },
            {
                value:'Lat Oblique "Y"',
                selected:false
            }
        ]
    },
    10:{
        name:"Skull",
        options:[
            {
                value:"OF",
                selected:false,
            },
            {
                value:"Lat",
                selected:false
            },
            {
                value:"Towne's",
                selected:false
            },
            {
                value:"Reverse Towne's",
                selected:false
            },
            {
                value:"SMV",
                selected:false
            }
        ]
    },
    11:{
        name:"Sinuses",
        options:[
            {
                value:"OF",
                selected:false
            },
            {
                value:"Lat",
                selected:false
            },
            {
                value:"OM",
                selected:false
            }
        ]
    },
    12:{
        name:"Temporamandibular (TMJ)",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            },
            {
                value:"Open",
                selected:false
            },
            {
                value:"Close",
                selected:false
            },
            {
                value:"Towne's",
                selected:false,
            }
        ]
    },
    13:{
        name:"Mandible",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            },
            {
                value:"Oblique",
                selected:false
            },
            {
                value:"OF",
                selected:false
            }
        ]
    },
    14:{
        name:"Mastoids/IAM",
        options:[
            {
                value:"Oblique",
                selected:false
            },
            {
                value:"Profile",
                selected:false
            },
        ]
    },
    15:{
        name:"Sella Turcica",
        options:[
            {
                value:"Leteral cone view",
                selected:false
            },
            {
                value:"OFF 20<sup>0</sup>",
                selected:false
            },

        ]
    },
    16:{
        name:"Orbits",
        options:[
            {
                value:"OM",
                selected:false
            },
            {
                value:"OF",
                selected:false
            }
        ]
    },
    17:{
        name:"Optic Foramina",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            },
            {
                value:"Oblique",
                selected:false
            }
        ]
    },
    19:{
        name:"Jugular Foramina",
        options:[
            {
                value:"SMV",
                selected:false
            }
        ]
    },
    20:{
        name:"Post Nasal Space",
        options:[]
    },
    21:{
        name:"Pharynx/Upper way",
        options:[]
    },
    22:{
        name:"Thoracic Inlet",
        options:[]
    },
    23:{
        name:"Pelvis",
        options:[]
    },
    24:{
        name:"Hip joint",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    25:{
        name:"Femur Thigh",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    26:{
        name:"Knee Joint",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    27:{
        name:"Leg (Tibia/Fibula)",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    28:{
        name:"Ankle Joint",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            },
            {
                value:"AP",
                selected:false
            },
            {
                value:"Lat",
                selected:false
            },
            {
                value:"Mortise",
                selected:false
            }
        ]
    },
    29:{
        name:"Foot",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    30:{
        name:"Calcaneum",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            },
            {
                value:"Axial",
                selected:false
            },
            {
                value:"Lateral",
                selected:false
            }
        ]
    },
    31:{
        name:"Wrist Joint",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    32:{
        name:"Elbow Joint",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    33:{
        name:"Fore arm (Radius/Ulna)",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    34:{
        name:"Arm (Humerus)",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    35:{
        name:"Abdomen",
        options:[
            {
                value:"RT",
                selected:false
            },
            {
                value:"LT",
                selected:false
            }
        ]
    },
    36:{
        name:"Skeletal Survey",
        options:[
            {
                value:"Supine",
                selected:false
            },
            {
                value:"Erect",
                selected:false
            },
            {
                value:"KUB",
                selected:false
            }
        ]
    },
    37:{
        name:"Instravenous Urography(IVU) - Non-Ionic Contrast",
        options:[]
    },
    38:{
        name:"Hysterosalpingography (HSG)",
        options:[]
    },
    39:{
        name:"Barium Swallow",
        options:[]
    },
    40:{
        name:"Barium Meal",
        options:[]
    },
    41:{
        name:"Barium Meal & Follow Through",
        options:[]
    },
    42:{
        name:"Double Contrast Barium Enema",
        options:[]
    },
    43:{
        name:"Retrograde Urethrocystography (RUCG)",
        options:[]
    },
    44:{
        name:"Micturating Urethrocysterography (MUCCG)",
        options:[]
    },
    45:{
        name:"RUCG + MUCG",
        options:[]
    },
    46:{
        name:"Fistulography",
        options:[]
    },
    47:{
        name:"Doppler/Vascular Studies",
        options:[]
    },
    48:{
        name:"Abdominopelvic Scan",
        options:[]
    },
    49:{
        name:"Pelvic Scan",
        options:[]
    },
    50:{
        name:"Abdominal",
        options:[]
    },
    51:{
        name:"Transvaginal",
        options:[]
    },
    52:{
        name:"Throid/Neck Scan",
        options:[]
    },
    53:{
        name:"Breast USS",
        options:[]
    },
    54:{
        name:"Scrotal USS",
        options:[]
    },
    55:{
        name:"Transcretal USS",
        options:[]
    },
    56:{
        name:"Ocular Scan",
        options:[]
    },
    57:{
        name:"Obsteric Scan",
        options:[]
    },
    58:{
        name:"Prostate Scan",
        options:[]
    },
    59:{
        name:"Biophysical Profile",
        options:[]
    },
    60:{
        name:"Musculoskeletal Scan",
        options:[]
    },
    61:{
        name:"USS Guided drainage",
        options:[]
    },
    62:{
        name:"USS Guided biospy",
        options:[]
    },
    63:{
        name:"Follicular Tracking",
        options:[]
    },
    64:{
        name:"Transfontanelle Scan",
        options:[]
    },
    65:{
        name:"Anomaly Scan",
        options:[]
    },
    66:{
        name:"Renal Scan",
        options:[]
    }

};

export  { radiologicalTests }