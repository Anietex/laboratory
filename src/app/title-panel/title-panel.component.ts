import {Component, Input, OnInit,} from '@angular/core';

@Component({
  selector: 'app-title-panel',
  template: `
      <div class="card-panel app-title animated slideInDown fast">
          <div class="card-title">
              <h5 class="title">{{ title }}</h5>
          </div>
          <div class="card-content">
              <ul class="breadcrumbs">
                  <ng-content></ng-content>
              </ul>
          </div>
      </div>
  `,
  styles: []
})
export class TitlePanelComponent implements OnInit {
  @Input() title: string;
  @Input() breadcrumb: string;
  constructor() { }

  ngOnInit() {
  }

}
