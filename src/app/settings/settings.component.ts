import { Component, OnInit, AfterViewInit,ViewChild, ElementRef } from '@angular/core';
import {FormGroup, FormControl } from '@angular/forms';
import { AfterSaveActions } from '../shared/AfterSaveActions';
import {SettingService} from './setting.service';
import  MiniToast from '../../MiniToast';


declare let M:any;

@Component({
  selector: 'app-settings-component',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, AfterViewInit {


  @ViewChild('uploadInput') uploadInput: ElementRef;
  @ViewChild('logoView') logoView: ElementRef;

   loading = false;
   toast = new MiniToast();
   labName = new FormControl('');
   subName = new FormControl('');
   saveAction = new FormControl('');
   phoneNo = new FormControl('');
   labAddress = new FormControl('')
   email = new FormControl('')
   logoUrl = '';




  afterSaveAction = AfterSaveActions;

  constructor(private serviceProvider: SettingService) { }

  ngOnInit() {
    this.getSettings();
  }

  ngAfterViewInit(){
  	this.initSelect();
  }

  initSelect(){
     let elems = document.querySelectorAll('select');
     M.FormSelect.init(elems);
  }


  showFileDialog(){
    this.uploadInput.nativeElement.click();
  }

  onImageSelected(event){
    let files = event.target.files;
      if(files.length>0){
        if(files[0].type.startsWith('image/')){
           let reader = new FileReader();
            reader.onload = e =>{
              let ev = e as any;
            this.logoView.nativeElement.src = ev.target.result ;
      }
      reader.readAsDataURL(files[0])
        }
      }
  }


  updateSettings(event){
    this.loading = true;
    let formData = new FormData(event.target);
    this.serviceProvider.UpdateSetting(formData).subscribe(
      (data)=>{
        this.loading = false;
        this.toast.success("Setting was updated successfully");
         this.getSettings();
      },

      (error)=>{
        this.loading = false;
        if(error.status ==0){
          this.toast.error("Can't connect to the server please try again")
        }else{
          this.toast.error("An error occured Settings  could not be saved, please try again");
        }
      });


  }


  getSettings(){
      this.loading = true;
      this.serviceProvider.getSettings().subscribe(
        (data)=>{
          this.loading = false;
          let d = data as any;
          this.labName.patchValue(d.lab_name);
          this.subName.patchValue(d.sub_name);
          this.saveAction.patchValue(d.after_save_action);
          this.labAddress.patchValue(d.address);
          this.phoneNo.patchValue(d.phone_no);
          this.email.patchValue(d.email);
          this.logoUrl = d.logo_file;
          this.loading = false;
          this.serviceProvider.saveLocalSettings(data);
           M.updateTextFields();
            this.initSelect();
        },

        (error)=>{
          this.getSettings();
        });
  }


      

     


}
