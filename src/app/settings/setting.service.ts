import { Injectable,Inject, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

	public onSettingsUpdated = new EventEmitter<any>();

  constructor(@Inject('API_URL') private appUrl: string, private http: HttpClient ) {

   }


  UpdateSetting(formData: FormData){
  	return this.http.post(this.appUrl+'settings/update',formData);
  }

  getSettings(){
  	return this.http.get(this.appUrl+'settings');
  }



  emitSettingUpdated(){
  	this.onSettingsUpdated.emit();
  }

  saveLocalSettings(setting){
  	localStorage.setItem('settings',JSON.stringify(setting));
  	this.emitSettingUpdated();
  }

  getLocalSettings(){
  	return  JSON.parse(localStorage.getItem('settings'));
  }




}
