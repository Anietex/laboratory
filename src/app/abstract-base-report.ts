import {IReportBasicInformation} from './interfaces/ireport-basic-information';
import MiniToast from '../MiniToast';
import {FormGroup} from '@angular/forms';
import {AfterViewInit, OnInit} from '@angular/core';

export abstract   class AbstractBaseReport implements OnInit, AfterViewInit {
    protected basicInfo: IReportBasicInformation = {
        lab_id:'',
        
       
       
        
      
      
      
        requested_by: '',
        clinical_detail:'',
      
        exam_required : '',
        specimen : '',
        clinic : '',

    };
    public abstract showBasicInfo: boolean;
    public abstract hideNextBtn: boolean;
    public abstract hidePrevBtn: boolean;
    public abstract showSave: boolean;
    public abstract BASIC_INFO_STORAGE: string;
    public abstract TEST_FORM: FormGroup;
    protected abstract DATA_STORAGE: string;
    public showLoading = false;
    protected savedFormValue = true;
    public activePane = 0;


    abstract showPane(): void;


    abstract saveReport(event: Event): void;



    protected saveTempData(value: any) {
        if (this.savedFormValue) {
          localStorage.setItem(this.DATA_STORAGE, JSON.stringify(value));
        }
    }

    ngOnInit() {
        this.TEST_FORM.valueChanges.subscribe(value => {
            this.saveTempData(value);
        });
    }

    ngAfterViewInit() {
        this.getTempData();
    }

    getTempData() {
        if (this.savedFormValue) {
          this.TEST_FORM.patchValue({...JSON.parse(localStorage.getItem(this.DATA_STORAGE))});
        }

    }

    resetForm(): void {
        this.TEST_FORM.reset();
        localStorage.removeItem(this.DATA_STORAGE);
        localStorage.removeItem(this.BASIC_INFO_STORAGE);

        const keys = Object.keys(this.basicInfo);

        for(const key of keys){
               this.basicInfo[key]='';
        }
    }

    nextPane() {
        this.activePane++;
        this.showPane();
    }

    prevPane() {
        this.activePane--;
        this.showPane();
    }


    getBasicInfo(info: IReportBasicInformation) {
        // this.basicInfo.surname = info.surname;
        // this.basicInfo.middle_name = info.middle_name;
        // this.basicInfo.first_name = info.first_name;
        // this.basicInfo.age = info.age;
        // this.basicInfo.sex = info.sex;
        // this.basicInfo.date = info.date;
        // this.basicInfo.time = info.time;
        // this.basicInfo.requested_by = info.requested_by;
        // this.basicInfo.email = info.email;
        // this.basicInfo.exam_required = info.exam_required;
        // this.basicInfo.specimen = info.specimen;
        // this.basicInfo.clinic = info.clinic;
        // this.basicInfo.lab_no = info.lab_no;
        // this.basicInfo.clinical_detail = info.clinical_detail;
    }

    protected formIsValid() {
        let isValid = true;

        const keys: string[] = Object.keys(this.basicInfo);

        for (const key of keys) {
          
            if (typeof this.basicInfo[key]=="string" && this.basicInfo[key].trim() === '') {
                isValid = false;
                break;
            }
        }

        return isValid;

    }


    protected patchBasicForm(report: IReportBasicInformation){
        // const basicInfo: IReportBasicInformation = {
        //       lab_no: report.lab_no,
        //       surname: report.surname,
        //       middle_name: report.middle_name,
        //       first_name: report.first_name,
        //       age: report.age,
        //       sex: report.sex,
        //       date: report.date,
        //       time: report.time,
        //       requested_by: report.requested_by,
        //       email: report.email,
        //       exam_required: report.exam_required,
        //       specimen: report.specimen,
        //       clinic: report.clinic,
        //       clinical_detail:report.clinical_detail,

        // };
        // this.basicInfo = basicInfo;
    }

}
