import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	recent:any ;
	total: any;
  loadingDetails =false;
  serverError = false;
  conError = false;

  constructor(private service:DashboardService) { 
    this.getSummary = this.getSummary.bind(this);

  }

  ngOnInit() {
      this.getSummary();
  	
  }


  getSummary(){
    this.loadingDetails =true;
    this.serverError = false;
    this.conError = false;
    this.service.getSummary().subscribe((data:any)=>{
      this.recent = data.recent;
      this.total = data.total;
      this.loadingDetails =false;
    },(err)=>{
        this.loadingDetails =false;
           if(err.status == 0){
               this.conError = true;
           }else{
             this.serverError = true;
           }
    })
  }

}
