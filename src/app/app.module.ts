import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterializeDesignModule } from './materialize-design/materialize-design.module';


import '../assets/js/minitoast'
import '../../node_modules/materialize-css/dist/js/materialize';
import '../../node_modules/toastr/toastr'
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { TitlePanelComponent } from './title-panel/title-panel.component';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { HaematologyComponent } from './haematology/haematology.component';
import {AppRoutingModule} from './route/app-routing.module';

import { ReportBasicInfoComponent } from './shared/report-basic-info/report-basic-info.component';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';




// import { MedicalMicrobiologyComponent } from './medical-microbiology/medical-microbiology.component';
import { RadiologicalRequestComponent } from './radiological-request/radiological-request.component';
import { HaematologyReportsComponent } from './haematology/haemtology-reports/haematology-reports.component';
import { HaematologyReportViewComponent } from './haematology/haematology-report-view/haematology-report-view.component';
import { PrintedPageComponent } from './printed-page/printed-page.component';
// import { HaematologyEditComponent } from './haematology-edit/haematology-edit.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { ChemicalPathologyReportsComponent } from './chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component';
import { ChemicalPathologyEditComponent } from './chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component';
import { ChemicalPathologyViewComponent } from './chemical-pathology/chemical-pathology-view/chemical-pathology-view.component';
import { MedicalMicrobiologyReportsComponent } from './medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component';
// import { MedicalMicrobiologyEditComponent } from './medical-microbiology-edit/medical-microbiology-edit.component';
import { MedicalMicrobiologyViewComponent } from './medical-microbiology/medical-microbiology-view/medical-microbiology-view.component';
import { RadiologicalRequestListComponent } from './radiological-request-list/radiological-request-list.component';
import { RadiologicalRequestEditComponent } from './radiological-request-edit/radiological-request-edit.component';
import { RadiologicalRequestViewComponent } from './radiological-request-view/radiological-request-view.component';
import { CreatePatientComponent } from './patient/create-patient/create-patient.component';
import { PatientFormComponent } from './patient/patient-form/patient-form.component';
import { LinearLoadingComponent } from './shared/linear-loading/linear-loading.component';
import { ConnectionErrorComponent } from './shared/connection-error/connection-error.component';
import { PatientListComponent } from './patient/patient-list/patient-list.component';
import { ServerErrorComponent } from './shared/server-error/server-error.component';
import { EditPatientComponent } from './patient/edit-patient/edit-patient.component';
import { NoRecordComponent } from './shared/no-record.component';
import { HaematologyCreateReport } from './haematology/create-report/create-report.component';
import { ReportFormComponent as HaematologyReportForm  } from './haematology/report-form/report-form.component';
import { EditReportComponent as HaematologyEditReport } from './haematology/edit-report/edit-report.component';
import { CreateChemicalPathologyComponent } from './chemical-pathology/create-chemical-pathology/create-chemical-pathology.component';

import { ChemicalPathologyFormComponent } from './chemical-pathology/chemical-pathology-form/chemical-pathology-form.component';
import { CreateMedicalMicrobiologyComponent } from './medical-microbiology/create-medical-microbiology/create-medical-microbiology.component';
import { MedicalMicrobiologyFormComponent } from './medical-microbiology/medical-microbiology-form/medical-microbiology-form.component';
import { EditMedicalMicrobiologyComponent } from './medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component';
import { SettingsComponent } from './settings/settings.component';
import { AddLabComponent } from './ManageLabs/add-lab/add-lab.component';
import { EditLabComponent } from './ManageLabs/edit-lab/edit-lab.component';
import { LabFormComponent } from './ManageLabs/lab-form/lab-form.component';
import { LabListComponent } from './ManageLabs/lab-list/lab-list.component';
import { ManageLabsComponent } from './manage-labs/manage-labs.component';
import { LoginComponent } from './login/login.component';
import {AuthInterceptor } from './Auth/auth-interceptor';
import {HTTP_INTERCEPTORS } from '@angular/common/http';
import { PatientLogComponent } from './patient/patient-log/patient-log.component';
import { ModalComponent } from './shared/modal/modal.component';



@NgModule({
    declarations: [
        AppComponent,
        SidebarComponent,
        HeaderComponent,
        TitlePanelComponent,
        DashboardComponent,
        // HaematologyComponent,
        ReportBasicInfoComponent,
       
        // MedicalMicrobiologyComponent,
        RadiologicalRequestComponent,
        HaematologyReportsComponent,
        HaematologyReportViewComponent,
        PrintedPageComponent,
        // HaematologyEditComponent,
        LoadingComponent,
        ChemicalPathologyReportsComponent,
        ChemicalPathologyEditComponent,
        ChemicalPathologyViewComponent,
        MedicalMicrobiologyReportsComponent,
        // MedicalMicrobiologyEditComponent,
        MedicalMicrobiologyViewComponent,
        RadiologicalRequestListComponent,
        RadiologicalRequestEditComponent,
        RadiologicalRequestViewComponent,
        CreatePatientComponent,
        PatientFormComponent,
        LinearLoadingComponent,
        ConnectionErrorComponent,
        PatientListComponent,
        ServerErrorComponent,
        EditPatientComponent,
        NoRecordComponent,
        HaematologyCreateReport,
        HaematologyReportForm,
        HaematologyEditReport,
        CreateChemicalPathologyComponent,
        ChemicalPathologyFormComponent,
        CreateMedicalMicrobiologyComponent,
        MedicalMicrobiologyFormComponent,
        EditMedicalMicrobiologyComponent,
        SettingsComponent,
        AddLabComponent,
        EditLabComponent,
        LabFormComponent,
        LabListComponent,
        ManageLabsComponent,
        LoginComponent,
        PatientLogComponent,
        ModalComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MaterializeDesignModule

    ],
    providers: [{provide:'API_URL',useValue:"http://localhost/laboratory/api/"},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
