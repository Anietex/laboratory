import { Injectable, Inject } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ILaboratory } from './ilaboratory';

 
@Injectable({
  providedIn: 'root'
})

export class LabsService {

	

  constructor(@Inject('API_URL') private apiUrl: string, private http: HttpClient) { }

  public addLab(lab: ILaboratory){

  	let formData = new FormData();

  	formData.append('lab-name',lab.lab_name);
  	formData.append('location',lab.location);
  	formData.append('username',lab.username);
  	formData.append('password',lab.password);
  	
  	return this.http.post(this.apiUrl+'laboratories',formData)

  }

  updateLab(lab:ILaboratory){

  	let formData = new FormData();

  	formData.append('_method','PATCH');
  	formData.append('lab-name',lab.lab_name);
  	formData.append('location',lab.location);
  	formData.append('username',lab.username);

  	return this.http.post(this.apiUrl+'laboratories/'+lab.id,formData);

  }

  deleteLab(id:number){
  	let data = new FormData();
  	data.append('_method','DELETE');

  	return this.http.post(this.apiUrl+'laboratories/'+id,data);
  }

  getLabs(){
  	return this.http.get(this.apiUrl+'laboratories');
  }


  updatePassword(password:string,id:number){
  	let formData = new FormData();

  	formData.append('_method','PATCH');
  	formData.append('password',password);
  	return this.http.post(this.apiUrl+'laboratories/change-password/'+id,formData);

  }




}
