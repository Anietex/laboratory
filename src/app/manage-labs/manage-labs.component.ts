import { Component, OnInit, AfterViewInit ,ElementRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {ViewChild } from '@angular/core';
import MiniToast from '../../MiniToast';
import { LabsService } from './labs.service';
import { AbstractTableComponent } from '../shared/abstract-table-component';
import {ILaboratory } from './ilaboratory';



declare const M:any;
declare const roar:  any;


@Component({
  selector: 'app-manage-labs',
  templateUrl: './manage-labs.component.html',
  styleUrls: ['./manage-labs.component.scss']
})

export class ManageLabsComponent extends AbstractTableComponent implements OnInit, AfterViewInit  {

	modalTitle = "New Lab";
	modalBtnText = "Add Lab";
	updating = false;
	loadingLabs = false;
	activeID:number;
	labs: ILaboratory[];
	activeLab : ILaboratory;
	deleting =false;
	changingPassword =false;

	@ViewChild('labmodal') labModal: ElementRef;

	@ViewChild('password') passwordModal: ElementRef;
	toast = new MiniToast();

	loading = false;

	public labForm = new FormGroup({
		lab_name :new FormControl(''),
		location: new FormControl(''),
		username: new FormControl(''),
		password: new FormControl(''),
		confirmPassword: new FormControl('')
	});

	public passwordForm = new FormGroup({
		password: new FormControl(),
		confirmPassword: new FormControl(),
	})

   columnsToDisplay = ['id','lab_name','location','username','password','edit','delete'];

   constructor(private serviceProvider: LabsService ) { 
   		super();
   		this.getLabs = this.getLabs.bind(this);
    }

    addLab(){
    	this.serviceProvider.addLab(this.labForm.value).subscribe(
    		(data)=>{
    			this.loading = false;
    			this.toast.success("Lab was added Successfully");
    			this.labForm.reset();
    			M.updateTextFields();
    			this.getLabs();
    		},
    		(error)=>{
				this.loading = false;

				console.log(error);

	  			if(error.status ==0){
	  				this.toast.error("Can't connect to server please try again");
	  			}else if(error.status ==422){

	  				if('username' in error.error.errors){
	  							this.toast.error("Username already exist");
	  						}

	  					if('lab_name' in error.error.errors){
	  							this.toast.error("Lab Name already exist");
	  					}
	  			}

	  			else{
	  			
	  				this.toast.error("An error occurred lab could not be added");

	  			}
    		});
    }


    updateLab(){
    	let lab = this.labForm.value as ILaboratory;
    	lab.id = this.activeLab.id;

    	this.serviceProvider.updateLab(lab).subscribe(
    		(data)=>{
    			this.loading = false;
    			this.toast.success("Lab was updated Successfully");
    			M.updateTextFields();
    			this.getLabs();
    		},
    		(error)=>{
				this.loading = false;
	  			if(error.status ==0){
	  				this.toast.error("Can't connect to server please try again");
	  			}
	  			else{
	  				this.toast.error("An error occurred lab could not be added");
	  			}
    		});
    }


    handleForm(){
    	if(this.labForm.valid){
    		if(this.labForm.value.password != this.labForm.value.confirmPassword){
    			this.toast.error("Password do not match");
    		}else{
    			this.loading = true;
    			if(this.updating){
    				this.updateLab();
    			}else{
    				this.addLab();
    			}
    		}

    	}else{
    		this.toast.error("Please complete All the required field");
    	}
    }

  ngOnInit() {
  	this.getLabs();
  }

  ngAfterViewInit(){

  	var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);
  }


  editLab(lab: ILaboratory){
  	this.updating = true;
  	this.modalTitle = "Update Lab Record";
  	this.modalBtnText = "Update";
  	this.activeLab  = lab;



  	let options = {
  		onCloseEnd:()=>{
    	this.updating = false;
    	this.modalTitle = "New Lab";
	    this.modalBtnText = "Add Lab";
	    this.labForm.reset();
	     M.updateTextFields();
    }
  	}

  	M.Modal.init(this.labModal.nativeElement,options, options);

    let modal = M.Modal.getInstance(this.labModal.nativeElement,options);

    this.labForm.patchValue(lab);

    this.labForm.patchValue({password:'null',confirmPassword:"null"});
    M.updateTextFields();
    

   
    modal.open();


  }

  deleteLab(id,index){
  	

  	 const toast = new MiniToast();
        
       var options = {
       cancel: true,
       cancelText: 'Cancel',
       cancelCallBack: function (event) {},
        confirm: true,
        confirmText: 'Delete',
        confirmCallBack: (event)=> {
            this.deleting = true;
            this.serviceProvider.deleteLab(id).subscribe((res)=>{
                toast.success('Lab record was deleted succesfully');
                this.deleting = false;
                this.labs.splice(index,1);

                this.matDatasource.data = this.labs;
        		this.matDatasource.paginator = this.paginator;

            },
            (err)=>{
                toast.error("Lab record could not be deleted at the moment please try again")
            })
           }
        
        }

        roar('Delete Record', 'Do you realy want to  <strong>Delete </strong> this record', options);
    }


    changePassword(id:number){
    	let modal = 	M.Modal.init(this.passwordModal.nativeElement);
    	modal.open();
    	this.activeID = id;
    }

    updatePassword(){
    	if(this.passwordForm.valid){
    		if(this.passwordForm.value.password != this.passwordForm.value.confirmPassword){
    			this.toast.error("Password do not match");
    		}else{
    			this.changingPassword = true;
    			this.serviceProvider.updatePassword(this.passwordForm.value.password,this.activeID)
    			.subscribe(
    				(data)=>{

    					this.changingPassword = false;
    					this.toast.success("Password updated Successfully");
    					this.passwordForm.reset();
    			
    				},
    				(err)=>{
    					this.changingPassword = false;
	  					if(err.status ==0){
	  						this.toast.error("Can't connect to server please try again");
	  					}
	  					else{
	  						this.toast.error("An error occurred password could not be updated");
	  					}
    				});
    		}

    	}else{
    		this.toast.error("Please complete All the required field");
    	}
    }


  getLabs(){
  	this.loadingLabs = true;
  	this.conError = false;
    this.serverError = false;
  	this.serviceProvider.getLabs().subscribe((data)=>{
  		this.labs = data as ILaboratory[];
  		this.matDatasource.data = this.labs;
        this.matDatasource.paginator = this.paginator;
        this.loadingLabs = false;
  	},
  	(err)=>{

  		 this.loadingLabs = false;
         if(err.status ==0){
                this.conError = true
            }else{
              this.serverError = true;
            }  
  	}
  	);

  }

}
