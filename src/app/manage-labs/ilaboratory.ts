export interface ILaboratory {
	id?:number;
	lab_name:string;
	location:string;
	username:string;
	password: string;
}
