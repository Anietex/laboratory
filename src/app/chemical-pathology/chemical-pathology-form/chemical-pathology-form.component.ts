import { Component, OnInit,Output, Input,EventEmitter,OnChanges } from '@angular/core';
import {AbstractReportForm } from '../../shared/abstract-report-form'
import {FormGroup, FormControl } from '@angular/forms';
import {PatientService } from '../../patient/patient.service';
import {IChemicalPathology } from '../ichemical-pathology';
import  Minitoast from '../../../MiniToast';
import { Router, ActivatedRoute } from '@angular/router';
declare let M:any;

@Component({
  selector: 'chemical-pathology-form',
  templateUrl: './chemical-pathology-form.component.html',
  styleUrls: ['./chemical-pathology-form.component.scss']
})
export class ChemicalPathologyFormComponent extends AbstractReportForm implements OnInit,OnChanges  {

 


 	@Output() submit = new EventEmitter<IChemicalPathology>();
 	@Input() report: IChemicalPathology;
   

 	 constructor(protected router: Router, protected patientService: PatientService, protected activeRoute: ActivatedRoute ) {
  		super(router,patientService,activeRoute);
   }


  chemPathForm = new FormGroup({
  	patient_id: new FormControl(''),
 	requested_by: new FormControl(),
 	clinical_detail: new FormControl(),
 	exam_required: new FormControl(),
 	specimen: new FormControl(),
 	clinic: new FormControl(),
  	total_cholesterol:new FormControl(''),
    triglyceride:new FormControl(''),
    hdlm:new FormControl(''),
    vldl: new FormControl(''),
    ldl: new FormControl(''),
    eucr: new FormControl(''),
    creatinine: new FormControl(''),
    urea:new FormControl(''),
    sodium:new FormControl(''),
    potassium:new FormControl(''),
    chloride:new FormControl(''),
    bicarbonate:new FormControl(''),
    fsh:new FormControl(''),
    lh:new FormControl(''),
    prolactin:new FormControl(''),
    progesteron: new FormControl(''),
    estradol:new FormControl(''),
    psa:new FormControl(''),
    ast:new FormControl(''),
    alt:new FormControl(''),
    alp:new FormControl(''),
    bilirubin:new FormControl(''),
    conj_bilirubin: new FormControl(''),
    total_protein: new FormControl(''),
    albunim: new FormControl(''),
    globulin: new FormControl(''),
    glucose: new FormControl(''),
    rbs:new FormControl(''),
    fbs:new FormControl(''),
    hrs2ppbs:new FormControl(''),
    amylase: new FormControl(''),
    cpk:new FormControl(''),
    gamma_glutamic: new FormControl(''),
    uric_acid: new FormControl(''),
    calcium:new FormControl(''),
    inorganic_phos: new FormControl(''),
    tsh:new FormControl(''),
    appearance: new FormControl(''),
    ph:new FormControl(''),
    protein:new FormControl(''),
    ketone_bodies:new FormControl(''),
    ur_bilirubin:new FormControl(''),
    nitric: new FormControl(''),
    ascorbic:new FormControl(''),
    leucocytes:new FormControl(''),
    specific_gravity:new FormControl(''),
    ur_glucose:new FormControl(''),
    urobilinogen:new FormControl(''),
    blood:new FormControl(''),
    crystals:new FormControl(''),
  });


  reset(){
  	this.chemPathForm.reset();
  	setTimeout(()=>{this.initMaterial(); M.updateTextFields();},200);
	this.activePane = 0;
	this.showPane();
  }


  showPane(){
  	if(this.activePane>3){
			this.showNextBtn = false;
			this.showSave = true;
		}else{
			this.showNextBtn = true;
			this.showSave = false;

		}
		 if(this.activePane >0 ){
			this.showPrevBtn = true;
		}else{
			this.showPrevBtn = false;
		}
  }



  handleSubmit(){
  	let patient_id = Number.parseInt(this.chemPathForm.value.patient_id)
		if(isNaN(patient_id)){
			new Minitoast().warning("You have not seleted any patient");
			this.activePane = 0;
			this.showPane();
		}else{
			this.submit.emit(this.chemPathForm.value)
		}
  }


  ngOnChanges(){
    console.log(this.report)
  	if(this.report){
  		this.chemPathForm.patchValue({...this.report});
  	}
  }

}
