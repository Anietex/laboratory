import { Component, OnInit,ViewChild  } from '@angular/core';
import {AbstractChemicalPathologyComponent } from '../abstract-chemical-pathology-component';
import { IChemicalPathology } from '../ichemical-pathology'; 
import { ChemicalPathologyService} from '../chemical-pathology.service';
import { SettingService} from '../../settings/setting.service'; 
import { AfterSaveActions } from '../../shared/AfterSaveActions';
import {ModalComponent } from '../../shared/modal/modal.component';

@Component({
  selector: 'app-create-chemical-pathology',
  templateUrl: './create-chemical-pathology.component.html',
  styleUrls: ['./create-chemical-pathology.component.scss']
})
export class CreateChemicalPathologyComponent extends AbstractChemicalPathologyComponent{

  @ViewChild(ModalComponent) modal : ModalComponent;


  constructor(private serviceProvider: ChemicalPathologyService,private setting: SettingService) { super() }

   onSubmit(report: IChemicalPathology){
     let settings = this.setting.getLocalSettings().after_save_action 
    if(settings == AfterSaveActions.ALWAYS_ASK){
       this.modal.openModal((options)=>{
         if(options){
           this.saveReport(report,options);
         }else{
            this.saveReport(report,{});
         }
       });
    }else if(settings == AfterSaveActions.SMS_PATIENT){
        let options = {
          sms:true
        }
         this.saveReport(report,options);

        this.saveReport(report,options);
    }else if(settings  == AfterSaveActions.EMAIL_PATIENT){
        let options = {
          email:true
        }
    }else if(settings == AfterSaveActions.EMAIL_AND_SMS_PATIENT){
       let options = {
          sms:true,
          email:true
        }
         this.saveReport(report,options);
    }else{
       this.saveReport(report,{});
    }

  	
  }





  saveReport(report,options){
      this.saving = true;
    this.serviceProvider.createReport(report,options).subscribe(
      (data)=>{
        this.saving = false;
        this.toast.success('Report was saved successfully')
        this.resetForm();
      },
      (error)=>{
        this.saving = false;
        if(error.status ==0){
          this.toast.error("Can't connect to server please try again");
        }else{
          this.toast.error("An error occurred report could not be saved");
        }
      }
      )
  }
 

}
