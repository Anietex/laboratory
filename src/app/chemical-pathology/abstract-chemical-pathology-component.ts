import { ChemicalPathologyFormComponent } from './chemical-pathology-form/chemical-pathology-form.component';
import  {ViewChild } from '@angular/core';
import MiniToast from '../../MiniToast';

export class AbstractChemicalPathologyComponent {

	@ViewChild(ChemicalPathologyFormComponent) reportForm;
	saving = false;
	toast = new MiniToast();
	
	public resetForm(){
		this.reportForm.reset();
	}
	
}
