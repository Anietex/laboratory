import { Injectable , Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IChemicalPathology} from './ichemical-pathology';

@Injectable({
  providedIn: 'root'
})
export class ChemicalPathologyService {

  constructor(@Inject('API_URL') private apiUrl: string,private http: HttpClient) { }


  createReport(report: IChemicalPathology,options){
     const formData = new FormData();
        let fields :string[] = Object.keys(report);
        for(const field of fields){
            if(field=='id') continue;
            formData.append(field,report[field]);
        }
        
      return  this.http.post(this.apiUrl+'chemical-pathology',formData,{params:options});
  }

  getReports(){
      return this.http.get(this.apiUrl+'chemical-pathology');
  }


  getReport(id: number){
        return this.http.get(this.apiUrl+'chemical-pathology/'+id)
  }

  updateReport(report: IChemicalPathology){
  	const formData = new FormData();
        formData.append('_method','PATCH');
        let fields :string[] = Object.keys(report);
        for(const field of fields){
            if(field=='id') continue;
            formData.append(field,report[field]);
        }

        return  this.http.post(this.apiUrl+'chemical-pathology/'+report.id,formData)
  }

  deleteReport(id: number){
       const formData = new FormData();
           formData.append('_method',"DELETE");
           return this.http.post(this.apiUrl+'chemical-pathology/'+id,formData);
  }
}
