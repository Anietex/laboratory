import { Component, OnInit } from '@angular/core';
import {IChemicalPathology } from '../ichemical-pathology';
import { ChemicalPathologyService } from '../chemical-pathology.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-chemical-pathology-view',
  templateUrl: './chemical-pathology-view.component.html',
  styleUrls: ['./chemical-pathology-view.component.scss']
})
export class ChemicalPathologyViewComponent implements OnInit {

	public report: IChemicalPathology;

   loadingReport = false;
    conError = false;
    serverError = false;
    id =null;

  constructor(private route: ActivatedRoute,private serviceProvider: ChemicalPathologyService) { 
     this.getReport = this.getReport.bind(this);
  }

  ngOnInit(){


    this.route.params.subscribe((param)=>{
          this.id = param.id;
         });


      this.getReport()
  }

  getReport(){
       this.loadingReport = true;
       this.conError = false;
       this.serverError = false;

         this.serviceProvider.getReport(this.id).subscribe((report)=>{
            this.loadingReport = false;
             const keys = Object.keys(report);

             for(const key of keys){
                 if( report[key] === null  || report[key] == 'null' ){
                     report[key]='';
                 }else if( report[key]===''){
                     report[key]='';
                 }
             }
            this.report = report as IChemicalPathology;

         },
         (err)=>{
            this.loadingReport = false;
           if(err.status == 0){
               this.conError = true;
           }else{
             this.serverError = true;
           }
         });
      }
      
  }
 

