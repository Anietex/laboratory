import { Component, OnInit } from '@angular/core';
import {AbstractChemicalPathologyComponent } from '../abstract-chemical-pathology-component';
import { IChemicalPathology } from '../ichemical-pathology'; 
import { ChemicalPathologyService } from '../chemical-pathology.service';
import {ActivatedRoute} from '@angular/router'; 


@Component({
  selector: 'app-chemical-pathology-edit',
  templateUrl: './chemical-pathology-edit.component.html',
  styleUrls: ['./chemical-pathology-edit.component.scss']
})
export class ChemicalPathologyEditComponent extends AbstractChemicalPathologyComponent implements OnInit {

     id= null;
      report :  IChemicalPathology;
      conError = false;
      serverError = false;
      loadingReport = false;

   constructor(private serviceProvider: ChemicalPathologyService, private route: ActivatedRoute) { super() }

   ngOnInit() {
      this.route.params.subscribe((params)=>{
          this.id = params.id;
          this.getReport();
      })
  }

   onSubmit(report: IChemicalPathology){
      this.saving = true;
      this.report = {...this.report,... report,};
      report.id = this.id;
      this.serviceProvider.updateReport(report).subscribe(
          (data)=>{
              this.saving = false;
              this.toast.success('Report was updated successfully')
          },
          (error)=>{
              this.saving = false;
              if(error.status ==0){
                  this.toast.error("Can't connect to server please try again");
              }else{
                  this.toast.error("An error occurred report could not be updated");
              }
          }
          )
      
  }

   getReport(){
      this.loadingReport = true;
      this.conError = false;
      this.serverError = false;
      this.serviceProvider.getReport(this.id).subscribe((report)=>{

          const keys = Object.keys(report);

             for(const key of keys){
                 if( report[key] === null  || report[key] == 'null' ){
                     report[key]='';
                 }else if( report[key]===''){
                     report[key]='';
                 }
             }
          this.report = report as IChemicalPathology;
          console.log(report);
          this.loadingReport = false;
      },
      (err)=>{
          this.loadingReport = false;
          if(err.status == 0){
              this.conError = true;
          }else{
              this.serverError = true;
          }
      }
      )
  }

}
