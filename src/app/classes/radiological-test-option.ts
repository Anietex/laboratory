import {IRadiologicalTestOption} from '../interfaces/iradiological-test-option';

export class RadiologicalTestOption implements IRadiologicalTestOption{

    value:string;
    selected:boolean;

    constructor(value: string){
        this.value = value;
    }

    public isSelected():boolean{
        return this.selected;
    }

   public toggleSelected(){
        this.selected = !this.selected;
    }

    public getName(){
        return this.value;
    }
}
