import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import MiniToast from '../../MiniToast';
import {AuthService } from '../Auth/auth.service';
import { Router } from '@angular/router';
declare let M:any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @ViewChild('passwordModal') passwordModal: ElementRef;

  toast = new MiniToast();
  loggingOut = false;

  constructor(private auth: AuthService, private router: Router) { 


  }

  ngOnInit() {

  }

  ngAfterViewInit(){
  	 var elems = document.querySelectorAll('.dropdown-trigger');
     M.Dropdown.init(elems,{constrainWidth:false})
  }


  logOut(){
    this.loggingOut = true;
  	this.toast.success("Logging you out now");

  	this.auth.logout().subscribe(
  	(res)=>{
  		this.auth.clearAuth();
  		this.toast.success("Logged out successfully");
  		this.router.navigateByUrl('/login');
      this.loggingOut = false;
  	},
  	(err)=>{
  		this.toast.error("An error, logout was not successful please try again");
       this.loggingOut = false;
  	});


  }

  changePassword(){
  	let modal = M.Modal.init(this.passwordModal.nativeElement);
  	modal.open();
  }


}
