import { Component, OnInit } from '@angular/core';
import {SettingService } from '../settings/setting.service';

@Component({
  selector: 'app-printed-page',
  templateUrl: './printed-page.component.html',
  styleUrls: ['./printed-page.component.scss']
})
export class PrintedPageComponent implements OnInit {

	labName = '';
	subName = '';
	logo='';
  email ='';
  address = '';
  phoneNo = '';

  constructor(private setting: SettingService) { 

  	this.labName = this.setting.getLocalSettings().lab_name;
  	this.subName = this.setting.getLocalSettings().sub_name;
  	this.logo = this.setting.getLocalSettings().logo_file;
    this.phoneNo = this.setting.getLocalSettings().phone_no;
    this.address = this.setting.getLocalSettings().address;
    this.email = this.setting.getLocalSettings().email;


   }

  ngOnInit() {
  }

  printPage(){
  	window.print()
  }



}
